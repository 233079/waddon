// ==UserScript==
// @name        Waddon Dev
// @version     0.1
// @author      kckckc
// @description Proxy userscript to load waddon
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Waddon
// @grant       GM_xmlhttpRequest
// @require     http://localhost:10001/waddon.user.js
// ==/UserScript==

// Run `yarn serve`
// Or change the @require to be a file url (much more reliable from my experience)
