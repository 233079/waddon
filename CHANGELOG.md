<!--

Changelog based on Keep a Changelog (https://keepachangelog.com/en/1.0.0/).

Added / Changed / Deprecated / Removed / Fixed

-->
# Changelog

## 3.4.0 - 2022-11-06
### Added
- Added Islebuilds inventory uploader

### Fixed
- Try to not break the online list if nicknames feature is disabled

## 3.3.1 - 2022-08-27
### Fixed
- Fixed Islebuilds display name feature breaking online list

## 3.3.0 - 2022-08-24
### Added
- Added option to show Islebuilds display name in chat

### Fixed
- Fixed possible NaN in timers with unintended input

## 3.2.3 - 2022-08-24
### Fixed
- Fixed pet names in Fjolgard Housing activating timers
- Fixed zone name text for minimap in Fjolgard Housing

## 3.2.2 - 2022-07-23
### Fixed
- Permanently hid timers channel

## 3.2.1 - 2022-07-23
### Added
- Character search in character select

### Fixed
- Fixed addon crashing on desktop client

## 3.2.0 - 2022-06-06
- Lots more refactoring.

### Added
- Added experimental chat replay feature.

### Changed
- Moved everything to the new menu.
- Player context menu extension is now disabled by default.

### Removed
- Removed old floating options menu.

### Fixed
- Fixed inviting players from the player context menu.
- Fixed slash action "Invite Nearby".

## 3.1.0 - 2022-06-02
### Added
- Added timer synchronization
  - When you observe a boss being killed, your Waddon will broadcast to other Waddons
  - When you log in, your Waddon will ask other Waddons for timers
  - Timers are also cached locally until they are re-synced

### Changed
- Slightly changed timers text.

## 3.0.0 - 2022-05-12
- Renamed the project to Waddon (v3.0.0)
- Moved repository location (`Isleward-Modding/addons/waddon2` to `Isleward-Modding/addons/waddon`)
- Moved built userscript location in repo (`build/` to `dist/`)
- Lots of internal refactoring.

### Added
- Added a new button to the UI to open addon options menu.
- Added "open old menu" button to options menu.
- Added "reset" button to visual options.
- Added "hide menu button" to misc options.
- Added new Islebuilds setup flow.
- Added new console banner with version and menu hotkey.
- Added a message when first installing or updating the addon.

### Changed
- Restyled panels slightly.
- Panels now remember their positions.
- Panels now stay inside the edges of the window (#1).
- Pressing `N` now toggles the new menu.
- Pressing `B` now toggles the old menu (until it is removed).
- Moved "Visual" panel to new menu.
- Moved "Log Linked Items" option to new menu.
- Moved Islebuilds options to new menu.
- Updated Islebuilds uploader to work with the new Islebuilds site.
  - Old versions of Waddon are no longer able to upload to Islebuilds.

### Deprecated
- Old options menu (previously `N`, now `B`) will be replaced by the new options menu.

### Removed
- Islebuilds item manager support has been removed, it will be rebuilt later.
- Removed "Open QQ Calculator" link, might be rebuilt later.

### Fixed
- Fixed minimap scale <1 not working (#4).
- Fixed uploading PTR data to Islebuilds

## 2.5.1 - 2022-04-21
### Fixed
- Fixed addon not loading in userscript manager environments.

## 2.5.0 - 2022-04-20
### Added
- Added border shadow to all panels.

### Changed
- The most recently moved panel will appear above other panels.
- Panel header context menus will always appear above all panels.

### Removed
- Removed mention of Help page in slash command search.
