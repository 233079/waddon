import enabledList from 'waddon-enabled';

export interface WaddonFeature {
	layer?: string;

	init?: () => void;
	// cleanup?: () => void;
}

const layerOrder = ['core', 'feature'];
const layers: Record<string, WaddonFeature[]> = {};
let hasInited = false;

const registerFeature = (feat: WaddonFeature) => {
	let layer = feat.layer ?? 'feature';
	if (!layerOrder.includes(layer)) {
		console.log('Waddon -- unknown layer: ' + layer);
		layer = 'feature';
	}

	if (!layers[layer]) {
		layers[layer] = [];
	}
	layers[layer].push(feat);

	if (hasInited && feat.init) {
		feat.init();
	}
};

export const features = {
	init: async () => {
		enabledList.forEach((f) => registerFeature(f));

		for (let i = 0; i < layerOrder.length; i++) {
			const currentLayer = layerOrder[i];

			if (layers[currentLayer]) {
				// Wait for each layer to finish before init'ing the next layer
				await Promise.all(
					layers[currentLayer].map(async (f) => {
						if (f.init) {
							await f.init();
						}
					})
				);
			}
		}

		hasInited = true;
	},

	register: registerFeature,
};
