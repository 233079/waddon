// CSS module imports
declare module '*.css';

// Addon Manager addon
declare const ADDON_ID: string;

// Rollup define plugin
declare const __ADDONVERSION__: string;

// Should never be used directly, besides util/getWindow.ts
declare const unsafeWindow: any;

// Not supported everywhere yet
declare namespace CSS {
	interface PropertyDefinition {
		name: string;
		syntax?: string;
		inherits: boolean;
		initialValue?: string;
	}
	function registerProperty(
		propertyDefinition: PropertyDefinition
	): undefined;
}
