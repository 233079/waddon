import { useCallback, useEffect, useState } from 'react';
import { settings, WaddonSettings } from '../settings';

export const useSetting = <T extends keyof WaddonSettings>(
	key: T
): [
	WaddonSettings[T],
	(
		arg:
			| WaddonSettings[T]
			| ((prev: WaddonSettings[T]) => WaddonSettings[T])
	) => void
] => {
	// Keep a "internal" value for this hook
	const [value, setInternal] = useState<WaddonSettings[T]>(() => {
		return settings.getValue(key);
	});

	// Listen for others changing this key, and update internal
	useEffect(() => {
		const cb = (newValue: WaddonSettings[T]) => {
			setInternal(newValue);
		};

		settings.onKeyChanged(key, cb);

		return () => {
			settings.offKeyChanged(key, cb);
		};
	}, [key]);

	// Custom setter to set on the store
	// Will trigger the listener which will set internal
	const setterFn = useCallback(
		(
			update:
				| ((data: WaddonSettings[T]) => WaddonSettings[T])
				| WaddonSettings[T]
		) => {
			if (typeof update === 'function') {
				return settings.transactValue(key, update);
			}

			settings.setValue(key, update);
		},
		[key]
	);

	return [value, setterFn];
};
