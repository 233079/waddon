import { useEffect, useState } from 'react';
import { IwdObject, IwdObjectsModule, IwdPlayer } from '../isleward.types';
import { getPlayer } from '../util/getWindow';
import { useEvents } from './useEvents';
import { useRequire } from './useRequire';

export const useIwdPlayer = (): IwdPlayer | null => {
	const objects = useRequire<IwdObjectsModule>('js/objects/objects');
	const events = useEvents();
	const [player, setPlayer] = useState<IwdPlayer | null>(null);

	const [, rerender] = useState({});

	useEffect(() => {
		if (!events || !objects) return;

		const searchNow = () => {
			const player = objects.objects.find((o) => o.self);
			if (player) {
				// We can do it like this to use the diff sent by the server:
				//   setPlayer({ ...player, ...obj});
				// Or we can grab it from objects/objects so that the client has already properly
				//   interpreted the events
				setPlayer(player as IwdPlayer);

				// Set the state to something new so the component will re-render
				// Sometimes, setPlayer(player) with the player object from objects/objects
				//   won't trigger re-render because it's the same object
				rerender({});
			}
		};

		const onGetObject = (obj: IwdObject) => {
			// setTimeout for 0ms to allow objects/objects to process these changes first
			setTimeout(() => {
				// If this event is telling us about ourself or updating us, we should process it
				if (
					obj.self ||
					(obj.id && getPlayer() && obj.id == getPlayer()?.id)
				) {
					searchNow();
				}
			}, 0);
		};

		// Search once when events or objects changes
		searchNow();

		events.on('onGetObject', onGetObject);

		return () => {
			events.off('onGetObject', onGetObject);
		};
	}, [events, objects]);

	return player;
};
