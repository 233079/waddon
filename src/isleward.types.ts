import { IWDItem } from '@kckckc/isleward-util/isleward';
import slash from './core/slash';
import { sendMessage } from './util/sendMessage';

export interface Sprite {
	visible: boolean;
}

export interface IwdMouseMoverCpn {
	sprite: Sprite;
}

export interface IwdPropheciesCpn {
	list: string[];
}

export interface IwdStatsCpn {
	values: Record<string, number>;
}

export interface IwdObject {
	id: number;
	self?: boolean;
	name?: string;
	level?: number;
	serverId?: number;

	isRare?: boolean;

	zoneId?: string;
	zoneName?: string;

	x?: number;
	y?: number;

	sprite: Sprite;

	// Components
	prophecies?: IwdPropheciesCpn;
	mouseMover?: IwdMouseMoverCpn;
	stats?: IwdStatsCpn;

	destroyed?: boolean;
	destructionEvent?: string;

	chest?: unknown;
	canBeSeenBy?: string;
}

export interface IwdPlayer extends IwdObject {
	account: string;
	class: string;
	level: number;
	skinId: string;
	sheetName: string;
	cell: number;
	portrait: {
		x: number;
		y: number;
	};
	zoneName: string;
	prophecies: IwdPropheciesCpn;
	passives: {
		selected: number[];
	};
	inventory: {
		items: IWDItem[];
	};
	reputation: {
		list: {
			id: string;
			description: string;
			initialRep: number;
			name: string;
			noGainRep?: boolean;
			rep: number;
			tier: number;
			// rewards
			// tiers
		}[];
	};
}

export interface IwdAddons {
	events?: IwdEvents;
}

export interface IwdChatMessage {
	class: string;
	message: string;
	type: string;
	channel?: string;
	source?: string;
	subType?: string;
}

export interface IwdConnectedPlayer {
	zoneName: string;
	name: string;
	class: string;
	level: number;
	id: number;
}

export interface OnGetMapData {
	zoneId: string;
	clientObjects: any[];
	collisionMap: (0 | 1)[][];
	hiddenRooms: {
		area: [number, number][];
		clientObj: boolean;
		fog: string;
		height: number;
		width: number;
		interior: string;
		layer: number;
		layerName: string;
		name: string;
		properties: {
			fog?: string;
			interior?: string;
		};
		sheetName: null;
		x: number;
		y: number;
	}[];
	map: string[][][];
}

// Missing a few properties but mostly complete
export interface IwdSpellData {
	cd: number;
	cdMax: number;
	manaCost: number;
	threatMult: number;
	castTime: number;
	castTimeMax: number;
	needLos: boolean;
	type: string;
	range: number;
	statMult: number;
	damage: number;
	row: number;
	col: number;
	values: Record<string, number>;
	name: string;
	description: string;
	icon: [number, number];
	auto: boolean;
	element: string;
	quality: number;
	statType: string;
	baseDamage: number;
	id: number;
	active?: boolean;
	aura?: boolean;
}

export interface OnGetSpellCooldownsData {
	spell: number;
	cd: number;
}

export interface IwdEventsTypes {
	onGetObject: (obj: IwdObject) => void;
	onGetMap: (data: OnGetMapData) => void;
	onRezone: (data: { oldZoneId: string; newZoneId: string }) => void;

	onBuildIngameUis: () => void;
	onShowCharacterSelect: () => void;

	onBeforeChat: (data: {
		success: boolean;
		message: string;
		cancel: boolean;
	}) => void;
	onKeyDown: (key: string) => void;
	onKeyUp: (key: string) => void;

	onGetMessages: (data: { messages: IwdChatMessage[] }) => void;

	onGetConnectedPlayer: (
		data: IwdConnectedPlayer | IwdConnectedPlayer[]
	) => void;

	onBeforePlayerContext: (target: IwdObject, context: any) => void;

	onDoWhisper: (name: string) => void;

	onGetSpells: (data: IwdSpellData[]) => void;
	onGetSpellCooldowns: (data: OnGetSpellCooldownsData) => void;
	onSetTarget: (data: IwdObject | null) => void;

	// not typed
	onGetItems: () => void;
	onDestroyItems: () => void;
	onAfterRenderUi: (ui: any) => void;
	onGetDamage: (data: any) => void;
	onBuiltItemTooltip: (tooltip: any) => void;

	// waddon
	'waddon:onMinimapClicked': (data: { x: number; y: number }) => void;
}

export interface IwdEvents {
	on<U extends keyof IwdEventsTypes>(
		event: U,
		listener: IwdEventsTypes[U]
	): void;
	off<U extends keyof IwdEventsTypes>(
		event: U,
		listener: IwdEventsTypes[U]
	): void;
	emit<U extends keyof IwdEventsTypes>(
		event: U,
		...args: Parameters<IwdEventsTypes[U]>
	): void;
}

export interface IwdWindow extends Window {
	player?: IwdPlayer;
	addons?: IwdAddons;

	// TODO
	Waddon: {
		id: string;
		name: string;
		init: () => Promise<void>;

		sendMessage: typeof sendMessage;
		registerAction: typeof slash['registerAction'];
		unregisterAction: typeof slash['unregisterAction'];
	};

	require: (paths: string[], callback: (...args: any[]) => void) => void;
}

export interface IwdObjectsModule {
	objects: IwdObject[];
}

export interface IwdClientModule {
	request: (data: any) => void;
}

export interface IwdRendererModule {
	layers: Record<
		string,
		{
			visible: boolean;
		}
	>;
}
