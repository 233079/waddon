const colors = [
	['#ff4252', '#d43346'],
	['#ff6942', '#b34b3a'],
	['#ffeb38', '#db5538'],
	['#80f643', '#386646'],
	['#51fc9a', '#3f8d6d'],
	['#48edff', '#3a71ba'],
	['#a24eff', '#7a3ad3'],
	['#fc66f7', '#de43ae'],
];

// Adapted https://github.com/pixijs/pixijs/blob/80716b0e827cd6b1fe051e4a859f550f041a70a2/packages/utils/src/browser/hello.ts
const hello = () => {
	const [c0, c1] = colors[new Date().getHours() % 8];
	const prompt = `press [N] for options`;

	if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
		const args = [
			`%c  %c Waddon v${__ADDONVERSION__} %c  %c  ${prompt}`,
			`background: ${c0}; padding:5px 0;`,
			`color: ${c0}; background: #2d2136; padding:5px 0;`,
			`background: ${c0}; padding:5px 0;`,
			`color: ${c1};`,
		];

		console.log(...args);
	} else {
		console.log(`Waddon - v${__ADDONVERSION__} - ${prompt}`);
	}
};

export { hello };
