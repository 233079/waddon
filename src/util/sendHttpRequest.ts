import { ISLEBUILDS_API_URL } from '../config';

// Certain environments (userscript manager / electron client / AddonManager) have different access to making requests

export const sendPostRequest = (path: string, data: any, headers = {}) => {
	return new Promise<string>((resolve, reject) => {
		try {
			//@ts-expect-error gm_ functions not defined
			if (typeof GM_xmlhttpRequest !== 'undefined') {
				//@ts-expect-error gm_ functions not defined
				GM_xmlhttpRequest({
					url: `${ISLEBUILDS_API_URL}${path}`,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						...headers,
					},
					data: JSON.stringify(data),
					onload: (response: any) => {
						resolve(response.responseText as string);
					},
					onerror: reject,
				});
			} else if (typeof fetch !== 'undefined') {
				fetch(`${ISLEBUILDS_API_URL}${path}`, {
					method: 'post',
					headers: {
						'Content-Type': 'application/json',
						...headers,
					},
					body: JSON.stringify(data),
				})
					.then((res) => {
						if (res.status !== 200)
							throw new Error(res.status + '');

						return res.text();
					})
					.then(resolve)
					.catch(reject);
			} else {
				const msg = `Waddon -- Error posting to islebuilds api: Missing GM_xmlhttpRequest or fetch.`;
				reject(msg);
			}
		} catch (e) {
			reject(e);
		}
	});
};

export const sendGetRequest = (path: string, headers = {}) => {
	return new Promise<string>((resolve, reject) => {
		try {
			//@ts-expect-error gm_ functions not defined
			if (typeof GM_xmlhttpRequest !== 'undefined') {
				//@ts-expect-error gm_ functions not defined
				GM_xmlhttpRequest({
					url: `${ISLEBUILDS_API_URL}${path}`,
					method: 'GET',
					headers,
					onload: (response: any) => {
						resolve(response.responseText as string);
					},
					onerror: reject,
				});
			} else if (typeof fetch !== 'undefined') {
				fetch(`${ISLEBUILDS_API_URL}${path}`, { headers })
					.then((res) => {
						if (res.status !== 200)
							throw new Error(res.status + '');

						return res.text();
					})
					.then(resolve)
					.catch(reject);
			} else {
				const msg = `Waddon -- Error posting to islebuilds api: Missing GM_xmlhttpRequest or fetch.`;
				reject(msg);
			}
		} catch (e) {
			reject(e);
		}
	});
};
