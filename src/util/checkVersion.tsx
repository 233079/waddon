import { Button } from '@kckckc/isleward-util';
import React from 'react';
import renderer from '../core/renderer';
import { getIslebuildsPagePath } from '../features/islebuilds/islebuildsApi';
import { Popup } from '../components/Popup';
import { settings } from '../settings';

export const checkVersion = () => {
	const prevVersion = settings.getValue('version');

	settings.setValue('version', __ADDONVERSION__);

	if (prevVersion === undefined) {
		// First time installing
		renderer.addElement(
			'version-notice',
			<Popup
				id="version-notice"
				header={`Waddon v${__ADDONVERSION__} installed`}
			>
				<div style={{ marginBottom: '12px' }}>
					Waddon has been successfully installed!
				</div>
				<div style={{ marginBottom: '12px' }}>
					Press <span className="q4">N</span> to open the options
					menu.
				</div>
				<div style={{ marginBottom: '12px' }}>
					Would you like to finish setting up <br />
					Islebuilds character uploads?
				</div>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						columnGap: '12px',
					}}
				>
					<div style={{ flex: 1 }} />
					<Button
						onClick={() => {
							open(
								getIslebuildsPagePath('/waddon/connect'),
								'_blank'
							);
						}}
					>
						Finish Setup
					</Button>
					<Button
						negative
						onClick={() => {
							renderer.removeElement('version-notice');
						}}
					>
						Close
					</Button>
				</div>
			</Popup>
		);
	} else if (prevVersion !== __ADDONVERSION__) {
		// Version was updated
		renderer.addElement(
			'version-notice',
			<Popup
				id="version-notice"
				header={`Waddon updated to v${__ADDONVERSION__}`}
			>
				<div style={{ marginBottom: '12px' }}>
					A new version of Waddon was installed.
				</div>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						columnGap: '12px',
					}}
				>
					<div style={{ flex: 1 }} />
					<Button
						onClick={() => {
							open(
								'https://gitlab.com/Isleward-Modding/addons/waddon/-/blob/master/CHANGELOG.md',
								'_blank'
							);
						}}
					>
						Open Changelog
					</Button>
					<Button
						negative
						onClick={() => {
							renderer.removeElement('version-notice');
						}}
					>
						Close
					</Button>
				</div>
			</Popup>
		);
	}
};
