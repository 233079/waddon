import { IwdPlayer, IwdWindow } from '../isleward.types';

export const getWindow = (): IwdWindow => {
	// eslint-disable-next-line no-restricted-globals
	return typeof unsafeWindow === 'undefined' ? window : unsafeWindow;
};

export const getPlayer = (): IwdPlayer | undefined => {
	return getWindow().player;
};
