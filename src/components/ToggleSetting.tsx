import { CM } from '@kckckc/isleward-util';
import React, { ReactNode } from 'react';
import { useSetting } from '../hooks/useSetting';
import { SettingsMatching } from '../settings';

interface ToggleSettingProps {
	children?: ReactNode;
	setting: SettingsMatching<boolean>;
}

export const ToggleSetting: React.FC<ToggleSettingProps> = ({
	children,
	setting,
}) => {
	const [value, setValue] = useSetting(setting);

	return (
		<CM.Checkbox
			checked={value}
			onChange={() => {
				setValue((x) => !x);
			}}
		>
			{children}
		</CM.Checkbox>
	);
};
