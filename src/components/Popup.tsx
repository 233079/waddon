import React, { ReactNode } from 'react';
import { Panel, Button } from '@kckckc/isleward-util';
import renderer from '../core/renderer';

interface PopupProps {
	id: string;
	header?: ReactNode;
	children?: ReactNode;
}

export const Popup: React.FC<PopupProps> = ({
	id,
	header = 'Message',
	children,
}) => {
	return (
		<Panel
			header={header}
			onClose={() => {
				renderer.removeElement(id);
			}}
			openCentered
			style={{ minWidth: '200px' }}
		>
			<div style={{ margin: '16px' }}>{children}</div>
		</Panel>
	);
};

export const makePopup = (
	header: ReactNode,
	body: ReactNode,
	buttons?: ReactNode
) => {
	const id = 'popup' + Math.floor(Math.random() * 10000);

	// Version was updated
	renderer.addElement(
		id,
		<Popup id={id} header={header}>
			<div style={{ marginBottom: '12px' }}>{body}</div>
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					columnGap: '12px',
				}}
			>
				<div style={{ flex: 1 }} />
				{buttons}
				<Button
					negative
					onClick={() => {
						renderer.removeElement(id);
					}}
				>
					Close
				</Button>
			</div>
		</Popup>
	);
};
