import { CM } from '@kckckc/isleward-util';
import React, { ReactNode } from 'react';
import { getWindow } from '../util/getWindow';

export const LinkButton: React.FC<{ to: string; children: ReactNode }> = ({
	to,
	children,
}) => {
	return (
		<CM.Button
			onClick={() => {
				getWindow().open(to, '_blank');
			}}
		>
			{children}
		</CM.Button>
	);
};
