import { IwdEvents } from '../../isleward.types';
import { requireAsync } from '../../util/requireAsync';
import { sendMessage } from '../../util/sendMessage';

interface ChatEventData {
	success: boolean;
	message: string;
	cancel: boolean;
}

type TriggerType = string | ((input: ChatEventData) => boolean);
export interface Command {
	trigger: TriggerType;
	callback: (data: ChatEventData) => void;
}

const commands: Command[] = [];

const register = (
	trigger: TriggerType,
	callback: (data: ChatEventData) => void
) => {
	commands.push({ trigger, callback });
};

const onBeforeChat = (msgConfig: ChatEventData) => {
	for (let i = 0; i < commands.length; i++) {
		const cmd = commands[i];
		if (typeof cmd.trigger === 'function') {
			if (!cmd.trigger(msgConfig)) continue;
		} else {
			if (
				!msgConfig.message ||
				!msgConfig.message.startsWith(cmd.trigger)
			)
				continue;
		}

		cmd.callback(msgConfig);
		return;
	}
};

const init = async () => {
	const events: IwdEvents = await requireAsync('js/system/events');

	events.on('onBeforeChat', onBeforeChat);

	register('/help', () => {
		sendMessage(
			'Waddon adds additional commands: <br/>' +
				commands
					.filter((c) => typeof c.trigger === 'string')
					.map((c) => c.trigger)
					.join('<br />'),
			'color-yellowB',
			'info'
		);
	});
};

const commandsModule = { layer: 'core', init, register };

export default commandsModule;
