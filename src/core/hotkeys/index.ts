import menu from '../menu';
import { getWindow } from '../../util/getWindow';
import { settings } from '../../settings';

const mappings: Record<string, () => void> = {};

let comboStart = false;
let comboUsed = false;

const onKeyDown = (key: string) => {
	if (!key) return;

	if (key === 'n') {
		comboStart = true;
	}

	if (comboStart && mappings[key]) {
		comboUsed = true;
		mappings[key]();
	}
};

const onKeyUp = (key: string) => {
	if (!key) return;

	if (key === 'n') {
		// If we didn't do anything with the combo key, open the options menu
		if (!comboUsed) {
			menu.toggle();
		}

		comboStart = false;
		comboUsed = false;
	}
};

const register = (key: string, handler: () => void) => {
	mappings[key] = handler;
};

const init = () => {
	const events = getWindow()?.addons?.events;
	if (!events) return;

	events.on('onKeyDown', onKeyDown);
	events.on('onKeyUp', onKeyUp);
};

const hotkeys = {
	layer: 'core',

	init,
	register,
};

export default hotkeys;
