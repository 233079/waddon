import React, { ReactElement } from 'react';
import { createRoot, Root } from 'react-dom/client';
import { Providers } from '../../components/Providers';
import styles from './index.module.css';

// Set up container
let root: Root;
const init = () => {
	const waddonContainer = document.createElement('div');
	waddonContainer.className = `waddon-container ${styles.container}`;
	waddonContainer.style.zIndex = '10000';

	const uiContainer = document.querySelector('.ui-container');
	uiContainer?.appendChild(waddonContainer);

	root = createRoot(waddonContainer);
};

// Manage element registration
let elements: { key: string; el: ReactElement }[] = [];

const addElement = (key: string, el: ReactElement) => {
	elements.push({
		key,
		el,
	});
	render();
};

const removeElement = (key: string) => {
	elements = elements.filter((e) => e.key !== key);
	render();
};

const hasElement = (key: string) => {
	return !!elements.find((e) => e.key === key);
};

const render = () => {
	root.render(
		<Providers>
			{elements.map((e) => React.cloneElement(e.el, { key: e.key }))}
		</Providers>
	);
};

const renderer = {
	layer: 'core',

	init,
	addElement,
	hasElement,
	removeElement,
};

export default renderer;
