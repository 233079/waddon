import { SlashAction } from '.';
import { IwdClientModule, IwdObjectsModule } from '../../isleward.types';
import { getPlayer } from '../../util/getWindow';
import { requireAsync } from '../../util/requireAsync';
import { sendMessage } from '../../util/sendMessage';

export const getDefaultActions = async (): Promise<SlashAction[]> => {
	const client: IwdClientModule = await requireAsync('js/system/client');
	const objects: IwdObjectsModule = await requireAsync('js/objects/objects');

	return [
		{
			id: 'refresh',
			name: 'Refresh',
			callback: () => {
				// eslint-disable-next-line
				window.location = window.location;
			},
		},
		{
			id: 'refresh2',
			name: 'Logout',
			callback: () => {
				// eslint-disable-next-line
				window.location = window.location;
			},
		},
		{
			id: 'mainmenu:options',
			name: 'Open Options',
			callback: () => {
				const mainMenu = $('.uiMainMenu').data('ui');
				if (mainMenu) mainMenu.openOptions();
			},
		},
		{
			id: 'mainmenu:charselect',
			name: 'Character Select',
			callback: () => {
				const mainMenu = $('.uiMainMenu').data('ui');
				if (mainMenu) mainMenu.charSelect();
			},
		},
		{
			id: 'party:invitenearby',
			name: 'Party: Invite Nearby',
			callback: () => {
				objects.objects.forEach((o) => {
					if (o.prophecies && !o.self) {
						sendMessage(
							`inviting ${o.name} (lv${o.level})`,
							'color-yellowB'
						);

						client.request({
							cpn: 'social',
							method: 'getInvite',
							data: {
								targetId: o.serverId,
							},
						});
					}
				});
			},
		},
		{
			id: 'party:leave',
			name: 'Party: Leave Party',
			callback: () => {
				const partyUi = $('.uiParty').data('ui');
				if (partyUi) partyUi.leaveParty();
			},
		},
		{
			id: 'mount',
			name: 'Mount',
			callback: () => {
				const inv = $('.uiInventory').data('ui');
				const items = getPlayer()?.inventory?.items;
				if (inv && items) {
					const mount = items.find((i) => i.type === 'mount');
					if (mount) {
						inv.performItemAction(mount, 'useItem');
					}
				}
			},
		},
	];
};
