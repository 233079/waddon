import React, { ReactNode } from 'react';
import { LinkButton } from '../../components/LinkButton';
import renderer from '../../core/renderer';
import { settings } from '../../settings';
import stateModule from '../state';
import {
	HiddenButton,
	ButtonInBuffs,
	ButtonInBody,
	HideMenuCheckbox,
	toggle,
} from './MenuControls';

const menuElements: { key: string; category?: string; el: ReactNode }[] = [];

export const getElements = () => {
	return menuElements;
};

const addElement = ({
	key,
	category,
	el,
}: {
	key: string;
	category?: string;
	el: ReactNode;
}) => {
	menuElements.push({ key, category, el });
};

let prevHidden = false;
let prevState = 'login';

const reposition = () => {
	const state = stateModule.get();

	const isHidden = settings.getValue('addonMenu.misc.hideMenuButton');

	if (
		isHidden !== prevHidden ||
		state !== prevState ||
		!renderer.hasElement('menu')
	) {
		renderer.removeElement('menu');

		if (isHidden) {
			renderer.addElement('menu', <HiddenButton />);
		} else {
			if (state === 'game') {
				renderer.addElement('menu', <ButtonInBuffs />);
			} else {
				renderer.addElement('menu', <ButtonInBody />);
			}
		}
	}

	prevHidden = isHidden;
	prevState = state;
};

const init = () => {
	// Misc buttons
	addElement({
		key: 'wiki button',
		category: 'misc',
		el: <LinkButton to="https://wiki.isleward.com/">[wiki]</LinkButton>,
	});
	addElement({
		key: 'leaderboards button',
		category: 'misc',
		el: (
			<LinkButton to="https://islebuilds.com/leaderboard">
				[leaderboards]
			</LinkButton>
		),
	});
	addElement({
		key: 'hide menu checkbox',
		category: 'misc',
		el: <HideMenuCheckbox />,
	});

	// Load stored value
	prevHidden = settings.getValue('addonMenu.misc.hideMenuButton');

	// Initial
	reposition();

	// Reposition on changes
	stateModule.subscribe(reposition);
	settings.onKeyChanged('addonMenu.misc.hideMenuButton', reposition);
};

const menu = {
	layer: 'core',

	init,
	addElement,
	toggle,
};

export default menu;
