import { CM } from '@kckckc/isleward-util';
import React, { ReactNode } from 'react';
import { getElements } from './index';

export const Menu = () => {
	const els = getElements();

	const categories: Record<string, ReactNode> = {};

	els.forEach((e) => {
		if (e.category) {
			// Add to category
			if (!categories[e.category]) {
				categories[e.category] = [];
			}

			(categories[e.category] as ReactNode[]).push(e.el);
		} else {
			// Independent category
			categories[e.key] = e.el;
		}
	});

	return (
		<CM>
			<CM.Label>
				<div
					style={{
						textAlign: 'center',
						background:
							'linear-gradient(45deg, cyan, cyan, violet, blue)',
						WebkitBackgroundClip: 'text',
						WebkitTextFillColor: 'transparent',
					}}
				>
					Waddon v{__ADDONVERSION__}
				</div>
			</CM.Label>
			<CM.Button
				onClick={() =>
					open(
						'https://gitlab.com/Isleward-Modding/addons/waddon/issues'
					)
				}
			>
				[report an issue]
			</CM.Button>
			{categories?.main ?? null}
			{Object.entries(categories)
				.filter(([label]) => label !== 'main')
				.sort((a, b) => a[0].localeCompare(b[0]))
				.map(([label, contents]) => {
					return (
						<CM.Expand key={label} label={label}>
							{contents}
						</CM.Expand>
					);
				})}
			{/*
			<CM.Expand label="tools">
				<ToolsMenu state={state} />
			</CM.Expand>
			<CM.Expand label="misc">
				<MiscMenu />
			</CM.Expand> */}
		</CM>
	);
};
