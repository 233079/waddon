import { useEffect, useState } from 'react';
import { GameState } from '.';
import state from './index';

const useGameState = () => {
	const [value, setValue] = useState<GameState>('login');

	useEffect(() => {
		const cb = (newState: GameState) => {
			setValue(newState);
		};

		state.subscribe(cb);
		return () => {
			state.unsubscribe(cb);
		};
	});

	return value;
};

export { useGameState };
