import React from 'react';
import menu from '../../core/menu';
import { settings } from '../../settings';
import {
	colorBetween,
	COLOR_BAD,
	COLOR_GOOD,
	COLOR_WHITE,
} from '../../util/colorUtil';
import { getWindow } from '../../util/getWindow';
import { requireAsync } from '../../util/requireAsync';
import { IWD_SPELLS, STAT_CALC, stringifyStatValue, translate } from './data';
import { TooltipsMenu } from './TooltipsMenu';

let G: any;
let input: any;

const CARD_SET_SIZES: Record<string, string> = {
	"Tradesman's Pride": '10',
	"Runecrafter's Toil": '3/10/30',
	'The Other Heirloom': '3',
	'Godly Promise': '6',
	'Fangs of Fury': '20',
	'Benthic Incantation': '12',
	'Cheer and Spear': '3',
	"Wizard's Vice": '5',
};

const colorize = (text: string, color: [number, number, number]) => {
	return `<span style="color:rgb(${color.join(
		','
	)});display:contents">${text}</span> `;
};

const buildPercent = (percent: number) => {
	return colorize(
		`[${Math.round(percent * 100)}%]`,
		colorBetween(percent, COLOR_BAD, COLOR_GOOD)
	);
};

const updateTooltip = (el: any, item: any) => {
	const enabled = settings.getValue('tooltips.enabled');
	if (!enabled) return;

	const showCardSetSize = settings.getValue('tooltips.cardSetSize');

	if (
		showCardSetSize &&
		item.type === "Gambler's Card" &&
		CARD_SET_SIZES[item.name]
	) {
		el.append(
			G.div('waddonSetSize', 'Set Size: ' + CARD_SET_SIZES[item.name])
		);
	}

	const perfectionForceMaxLevel = settings.getValue(
		'tooltips.perfection.maxLevel'
	);
	const shortenStatNames = settings.getValue('tooltips.shortenStatNames');
	const showStatPerfection = settings.getValue('tooltips.perfection.stats');
	const showSpellPerfection = settings.getValue('tooltips.perfection.spells');
	const showPerfectionPercent = settings.getValue(
		'tooltips.perfection.showPercent'
	);
	const showPerfectionRange = settings.getValue(
		'tooltips.perfection.showRange'
	);
	const showPerfectionAverage = settings.getValue(
		'tooltips.perfection.showOverall'
	);

	// Don't interfere with comparisons
	if (input.isKeyDown('shift', true)) {
		return;
	}

	if (item.spell && showSpellPerfection) {
		let levelMult = item.level / 20;

		// Always use level 20 stats
		if (perfectionForceMaxLevel) {
			levelMult = 1;
		}

		let ranges;
		if (item.ability)
			ranges = IWD_SPELLS[item.spell.name.toLowerCase()].random;
		else ranges = item.spell.random;

		const lines: any[] = [];
		const perfections: any[] = [];

		Object.entries(ranges).forEach(([property, range]: any) => {
			const isInt = property.indexOf('i_') === 0;
			let useProperty = property;
			const minRange = range[0];
			const maxRange = range[1];

			let minRoll = minRange;
			let maxRoll = minRange + (maxRange - minRange) * levelMult;

			if (isInt) {
				useProperty = property.substr(2);
				minRoll = Math.round(minRoll);
				maxRoll = Math.round(maxRoll);
			} else {
				minRoll = ~~(minRoll * 100) / 100;
				maxRoll = ~~(maxRoll * 100) / 100;
			}

			// minRoll = clamp(minRoll, minRange, maxRange);
			// maxRoll = clamp(maxRoll, minRange, maxRange);

			const actualRoll = item.spell.values[useProperty];

			let perf = 1;
			if (minRoll !== maxRoll) {
				perf = (actualRoll - minRoll) / (maxRoll - minRoll);
				perfections.push(perf);
			}

			let out = '';

			if (showPerfectionPercent)
				out += buildPercent(minRoll !== maxRoll ? perf : 1);

			if (showPerfectionRange)
				out += colorize(
					`[${minRoll.toFixed(2)}-${maxRoll.toFixed(2)}] `,
					COLOR_WHITE
				);

			out += `${useProperty}: ${actualRoll}`;

			lines.push(`<span>${out}</span>`);
		});

		el.find('.damage').html(lines.join(''));

		const pLen = perfections.length;
		const pAvg = perfections.reduce((a, b) => a + b, 0) / pLen;

		if (showPerfectionAverage && pLen > 1)
			el.find(item.ability ? '.name' : '.spellName').prepend(
				buildPercent(pAvg)
			);
	}

	if (item.stats) {
		let level = item.originalLevel ?? item.level;

		if (perfectionForceMaxLevel) {
			level = 20;
		}

		const perfections: any[] = [];

		const tempStats = Object.assign({}, item.stats);
		const enchantedStats = item.enchantedStats || {};

		Object.keys(tempStats).forEach((s) => {
			if (enchantedStats[s]) {
				tempStats[s] -= enchantedStats[s];

				if (tempStats[s] <= 0) delete tempStats[s];

				tempStats['_' + s] = enchantedStats[s];
			}
		});

		const lines = Object.entries(tempStats)
			.map((s: any) => {
				let out = '';

				const isEnchanted = s[0][0] === '_';
				let stat = s[0];
				if (isEnchanted) stat = stat.substr(1);

				const value = s[1];

				const statData = STAT_CALC[stat];

				// For "armor" stat (only happens in implicits, which we don't handle yet, and also on Dead Man's Band)
				if (!statData) {
					out += `${stringifyStatValue(stat, value)} ${translate(
						stat,
						{ shortenStatNames }
					)}`;
					let rowClass = '';
					if (isEnchanted) {
						rowClass = ' enchanted';
					}
					return `<div class="${rowClass}">${out}</div>`;
				}

				let useData = statData.range;
				if (item.slot === 'twoHanded') {
					useData = statData.range2h;
				}

				let [min, max] = useData[level - 1];

				let perf = (value - min) / (max - min);
				// double roll hack
				if (perf > 1) {
					perf = (value - 2 * min) / (max - min);
				}

				perfections.push(perf);

				if (showStatPerfection && showPerfectionPercent) {
					out += buildPercent(perf);
				}

				if (showStatPerfection && showPerfectionRange) {
					if (stat.indexOf('CritChance') > -1) {
						min /= 20;
						min = ~~(min * 10) / 10;
						max /= 20;
						max = ~~(max * 10) / 10;
					}

					out += colorize(`[${min}-${max}] `, COLOR_WHITE);
				}

				let rowClass = '';
				if (isEnchanted) {
					rowClass = ' enchanted';
				}

				out += `${stringifyStatValue(stat, value)} ${translate(stat, {
					shortenStatNames,
				})}`;

				return `<div class="${rowClass}">${out}</div>`;
			})
			.sort((a, b) => {
				return (
					a.replace(' enchanted', '').length -
					b.replace(' enchanted', '').length
				);
			})
			.sort((a, b) => {
				if (
					a.indexOf('enchanted') > -1 &&
					b.indexOf('enchanted') === -1
				)
					return 1;
				else if (
					a.indexOf('enchanted') === -1 &&
					b.indexOf('enchanted') > -1
				)
					return -1;

				return 0;
			});

		const wrappedHtml =
			'<div class="space"></div><div class="line"></div><div class="smallSpace"></div>' +
			lines.join('') +
			'<div class="smallSpace"></div><div class="line"></div>';
		el.find('.stats').first().html(wrappedHtml);

		const pLen = perfections.length;
		const pAvg = perfections.reduce((a, b) => a + b, 0) / pLen;

		if (showStatPerfection && showPerfectionAverage && pLen > 1) {
			el.find('.name').prepend(buildPercent(pAvg));
		}
	}
};

const onBuiltItemTooltip = (tooltip: any) => {
	const item = $('.uiTooltipItem').data('ui').item;
	updateTooltip(tooltip, item);
};

const init = async () => {
	G = (
		await requireAsync('ui/templates/tooltipItem/buildTooltip/lineBuilders')
	).lineBuilders;
	input = await requireAsync('js/input');
	const events = getWindow()?.addons?.events;

	menu.addElement({
		key: 'tooltips',
		el: <TooltipsMenu />,
	});

	events?.on('onBuiltItemTooltip', onBuiltItemTooltip);
};

export default { init };
