const IWD_SLOTS: {
	slots: string[];
	chance: Record<string, number>;
	armorMult: Record<string, number>;
	getRandomSlot: (exclude?: string) => string;
} = {
	slots: [
		'head',
		'neck',
		'chest',
		'hands',
		'finger',
		'waist',
		'legs',
		'feet',
		'trinket',
		'oneHanded',
		'twoHanded',
		'offHand',
		'tool',
	],

	chance: {
		head: 85,
		neck: 45,
		chest: 100,
		hands: 90,
		finger: 40,
		waist: 80,
		legs: 100,
		feet: 90,
		trinket: 35,
		oneHanded: 60,
		twoHanded: 60,
		offHand: 40,
		tool: 0,
	},

	armorMult: {
		head: 0.2,
		chest: 0.4,
		hands: 0.1,
		legs: 0.2,
		feet: 0.1,
	},

	getRandomSlot: function (exclude?: string) {
		const chances = [];
		for (const c in this.chance) {
			if (c === exclude) continue;

			const rolls = this.chance[c];
			for (let i = 0; i < rolls; i++) chances.push(c);
		}

		return chances[~~(Math.random() * chances.length)];
	},
};

const slotArmorMult = IWD_SLOTS.armorMult;

const configMaterials = {
	plate: {
		attrRequire: 'str',
		armorMult: 1,
	},
	leather: {
		attrRequire: 'dex',
		armorMult: 0.6,
	},
	cloth: {
		attrRequire: 'int',
		armorMult: 0.35,
	},
};

const plateArmorMult = configMaterials.plate.armorMult;
const leatherArmorMult = configMaterials.leather.armorMult;
const clothArmorMult = configMaterials.cloth.armorMult;

const IWD_TYPES: Record<string, any> = {
	head: {
		Helmet: {
			sprite: [0, 0],
			material: 'plate',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.head * plateArmorMult,
			},
		},
		Cowl: {
			sprite: [0, 1],
			material: 'cloth',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.head * clothArmorMult,
			},
		},
		'Leather Cap': {
			sprite: [0, 2],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.head * leatherArmorMult,
			},
		},
		Facemask: {
			sprite: [0, 3],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.head * leatherArmorMult,
			},
		},
	},
	neck: {
		Pendant: {
			sprite: [1, 0],
			implicitStat: {
				stat: 'str',
				value: [1, 4],
			},
		},
		Amulet: {
			sprite: [1, 1],
			implicitStat: {
				stat: 'int',
				value: [1, 4],
			},
		},
		Locket: {
			sprite: [1, 2],
			implicitStat: {
				stat: 'dex',
				value: [1, 4],
			},
		},
		Choker: {
			sprite: [1, 3],
			implicitStat: {
				stat: 'regenHp',
				value: [2, 5],
			},
		},
	},
	chest: {
		Breastplate: {
			sprite: [2, 0],
			material: 'plate',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.chest * plateArmorMult,
			},
		},
		Robe: {
			material: 'cloth',
			sprite: [2, 1],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.chest * clothArmorMult,
			},
		},
		'Leather Armor': {
			sprite: [2, 2],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.chest * leatherArmorMult,
			},
		},
		Scalemail: {
			sprite: [2, 3],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.chest * leatherArmorMult,
			},
		},
	},
	hands: {
		Gauntlets: {
			sprite: [3, 0],
			material: 'plate',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.hands * plateArmorMult,
			},
		},
		Gloves: {
			material: 'cloth',
			sprite: [3, 1],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.hands * clothArmorMult,
			},
		},
		'Leather Gloves': {
			sprite: [3, 2],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.hands * leatherArmorMult,
			},
		},
		'Scale Gloves': {
			sprite: [3, 3],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.hands * leatherArmorMult,
			},
		},
	},
	finger: {
		Signet: {
			sprite: [4, 0],
			implicitStat: {
				stat: 'armor',
				value: [5, 15],
			},
		},
		Ring: {
			sprite: [4, 1],
			implicitStat: {
				stat: 'regenMana',
				value: [1, 5],
			},
		},
		Loop: {
			sprite: [4, 2],
			implicitStat: {
				stat: 'allAttributes',
				value: [1, 7],
			},
		},
		'Viridian Band': {
			sprite: [4, 3],
			implicitStat: {
				stat: 'physicalPercent',
				value: [1, 3],
			},
		},
	},
	waist: {
		Belt: {
			material: 'plate',
			sprite: [5, 0],
			implicitStat: {
				stat: 'armor',
				value: [10, 20],
			},
		},
		Sash: {
			material: 'cloth',
			sprite: [5, 1],
			implicitStat: {
				stat: 'manaMax',
				value: [1, 8],
			},
		},
		'Leather Belt': {
			material: 'leather',
			sprite: [5, 2],
			implicitStat: {
				stat: 'addCritChance',
				value: [10, 50],
			},
		},
		'Scaled Binding': {
			material: 'leather',
			sprite: [5, 3],
			implicitStat: {
				stat: 'vit',
				value: [2, 6],
			},
		},
	},
	legs: {
		Legplates: {
			material: 'plate',
			sprite: [6, 0],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.legs * plateArmorMult,
			},
		},
		Pants: {
			material: 'cloth',
			sprite: [6, 1],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.legs * clothArmorMult,
			},
		},
		'Leather Pants': {
			sprite: [6, 2],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.legs * leatherArmorMult,
			},
		},
		'Scale Leggings': {
			sprite: [6, 3],
			material: 'leather',
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.legs * leatherArmorMult,
			},
		},
	},
	feet: {
		'Steel Boots': {
			material: 'plate',
			sprite: [7, 0],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.feet * plateArmorMult,
			},
		},
		Boots: {
			material: 'cloth',
			sprite: [7, 1],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.feet * clothArmorMult,
			},
		},
		'Leather Boots': {
			material: 'leather',
			sprite: [7, 2],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.feet * leatherArmorMult,
			},
		},
		'Scale Boots': {
			material: 'leather',
			sprite: [7, 3],
			implicitStat: {
				stat: 'armor',
				valueMult: slotArmorMult.feet * leatherArmorMult,
			},
		},
	},
	trinket: {
		'Forged Ember': {
			sprite: [8, 0],
			implicitStat: {
				stat: 'armor',
				value: [25, 70],
			},
		},
		'Smokey Orb': {
			sprite: [8, 1],
			implicitStat: {
				stat: 'dodgeAttackChance',
				value: [1, 3],
			},
		},
		'Quartz Fragment': {
			sprite: [8, 2],
			implicitStat: {
				stat: 'elementArcanePercent',
				value: [3, 12],
			},
		},
		'Mystic Card': {
			sprite: [8, 3],
			implicitStat: {
				stat: 'magicFind',
				value: [3, 12],
			},
		},
		'Dragon Fang': {
			sprite: [8, 4],
			implicitStat: {
				stat: 'attackSpeed',
				value: [1, 5],
			},
		},
	},
	oneHanded: {
		Sword: {
			attrRequire: 'str',
			sprite: [9, 0],
			spellName: 'melee',
			spellConfig: {
				statType: 'str',
				statMult: 1,
				threatMult: 4,
				cdMax: 5,
				castTimeMax: 0,
				useWeaponRange: true,
				random: {
					damage: [1.47, 9.65],
				},
			},
			implicitStat: {
				stat: 'attackSpeed',
				value: [1, 5],
			},
		},
		Dagger: {
			attrRequire: 'dex',
			sprite: [9, 2],
			spellName: 'melee',
			spellConfig: {
				statType: 'dex',
				statMult: 1,
				cdMax: 3,
				castTimeMax: 0,
				useWeaponRange: true,
				random: {
					damage: [0.88, 5.79],
				},
			},
			implicitStat: {
				stat: 'addAttackCritChance',
				value: [10, 50],
			},
		},
		Wand: {
			attrRequire: 'int',
			sprite: [9, 8],
			spellName: 'projectile',
			spellConfig: {
				statType: 'int',
				statMult: 1,
				element: 'holy',
				cdMax: 5,
				castTimeMax: 0,
				manaCost: 0,
				range: 9,
				random: {
					damage: [1.17, 7.72],
				},
			},
			implicitStat: {
				stat: 'castSpeed',
				value: [1, 5],
			},
		},
		'Curved Dagger': {
			name: "Steelclaw's Bite",
			level: [18, 20],
			attrRequire: 'dex',
			quality: 4,
			slot: 'oneHanded',
			sprite: [1, 0],
			spritesheet: '../../../images/legendaryItems.png',
			type: 'Curved Dagger',
			description:
				'The blade seems to be made of some kind of bone and steel alloy.',
			stats: ['dex', 'dex', 'addCritMultiplier', 'addCritMultiplier'],
			effects: [
				{
					type: 'damageSelf',
					properties: {
						element: 'poison',
					},
					rolls: {
						i_percentage: [8, 22],
					},
				},
				{
					type: 'alwaysCrit',
					rolls: {},
				},
			],
			spellName: 'melee',
			spellConfig: {
				statType: 'dex',
				statMult: 1,
				cdMax: 3,
				castTimeMax: 0,
				useWeaponRange: true,
				random: {
					damage: [0.88, 5.79],
				},
			},
		},
	},
	twoHanded: {
		Trident: {
			name: "Princess Morgawsa's Trident",
			level: [18, 20],
			attrRequire: 'int',
			quality: 4,
			slot: 'twoHanded',
			sprite: [0, 0],
			spritesheet: '../../../images/legendaryItems.png',
			type: 'Trident',
			description:
				'Summoned from the ancient depths of the ocean by the Benthic Incantation.',
			stats: [
				'elementFrostPercent',
				'elementFrostPercent',
				'elementFrostPercent',
			],
			effects: [
				{
					type: 'freezeOnHit',
					rolls: {
						i_chance: [2, 5],
						i_duration: [2, 4],
					},
				},
			],
			spellName: 'projectile',
			spellConfig: {
				statType: 'int',
				statMult: 1,
				element: 'arcane',
				auto: true,
				cdMax: 7,
				castTimeMax: 0,
				manaCost: 0,
				range: 9,
				random: {
					damage: [1.65, 10.81],
				},
			},
		},
		Axe: {
			attrRequire: 'str',
			sprite: [9, 3],
			spellName: 'melee',
			spellConfig: {
				statType: 'str',
				statMult: 1,
				threatMult: 4,
				cdMax: 9,
				castTimeMax: 0,
				useWeaponRange: true,
				random: {
					damage: [2.64, 17.37],
				},
			},
			implicitStat: {
				stat: 'addAttackCritMultiplier',
				value: [10, 30],
			},
		},
		'Gnarled Staff': {
			attrRequire: 'int',
			sprite: [9, 1],
			spellName: 'projectile',
			spellConfig: {
				statType: 'int',
				statMult: 1,
				element: 'arcane',
				auto: true,
				cdMax: 7,
				castTimeMax: 0,
				manaCost: 0,
				range: 9,
				random: {
					damage: [1.65, 10.81],
				},
			},
			implicitStat: {
				stat: 'regenMana',
				value: [3, 9],
			},
		},
		Spear: {
			attrRequire: 'dex',
			sprite: [9, 6],
			spellName: 'melee',
			range: 2,
			spellConfig: {
				statType: 'dex',
				statMult: 1,
				threatMult: 4,
				cdMax: 6,
				castTimeMax: 0,
				useWeaponRange: true,
				random: {
					damage: [1.76, 11.58],
				},
			},
			implicitStat: {
				stat: 'dodgeAttackChance',
				value: [1, 7],
			},
		},
	},
	offHand: {
		'Wooden Shield': {
			attrRequire: 'str',
			sprite: [13, 0],
			implicitStat: [
				{
					stat: 'armor',
					valueMult: 0.3,
				},
				{
					stat: 'blockAttackChance',
					valueMult: 1,
				},
			],
		},
		'Gilded Shield': {
			attrRequire: 'str',
			sprite: [13, 1],
			implicitStat: [
				{
					stat: 'armor',
					valueMult: 0.6,
				},
				{
					stat: 'blockAttackChance',
					valueMult: 0.5,
				},
			],
		},
		'Brittle Tome': {
			attrRequire: 'int',
			sprite: [13, 2],
			implicitStat: {
				stat: 'addSpellCritChance',
				value: [10, 50],
			},
		},
		'Ancient Tome': {
			attrRequire: 'int',
			sprite: [13, 3],
			implicitStat: {
				stat: 'addSpellCritMultiplier',
				value: [10, 30],
			},
		},
	},
	tool: {
		'Fishing Rod': {
			sprite: [11, 0],
		},
	},
};

['Sickle', 'Jade Sickle', 'Golden Sickle', 'Bone Sickle'].forEach(function (i) {
	IWD_TYPES.oneHanded[i] = {
		// spritesheet: `${this.folderName}/images/items.png`,
		// sprite: [i, 0],
		spellName: 'melee',
		attrRequire: ['int'],
		spellConfig: {
			statType: ['str', 'int'],
			statMult: 1,
			cdMax: 4,
			castTimeMax: 0,
			useWeaponRange: true,
			random: {
				damage: [1, 7],
			},
		},
		implicitStat: {
			stat: 'lifeOnHit',
			value: [1, 30],
		},
	};
});

const IWD_SPELLS: Record<string, any> = {
	melee: {
		auto: true,
		cdMax: 10,
		castTimeMax: 0,
		useWeaponRange: true,
		random: {
			damage: [3, 11.4],
		},
	},
	projectile: {
		auto: true,
		cdMax: 10,
		castTimeMax: 0,
		manaCost: 0,
		range: 9,
		random: {
			damage: [2, 7.2],
		},
	},

	'magic missile': {
		statType: 'int',
		statMult: 1,
		element: 'arcane',
		cdMax: 7,
		castTimeMax: 6,
		manaCost: 5,
		range: 9,
		random: {
			damage: [4, 32],
		},
	},
	'ice spear': {
		statType: 'int',
		statMult: 1,
		element: 'frost',
		cdMax: 10,
		castTimeMax: 2,
		manaCost: 4,
		range: 9,
		random: {
			damage: [2, 15],
			i_freezeDuration: [6, 10],
		},
	},
	fireblast: {
		statType: 'int',
		statMult: 1,
		element: 'fire',
		cdMax: 4,
		castTimeMax: 2,
		manaCost: 5,
		random: {
			damage: [2, 10],
			i_radius: [1, 2.2],
			i_pushback: [2, 5],
		},
	},
	smite: {
		statType: 'int',
		statMult: 1,
		element: 'holy',
		cdMax: 6,
		castTimeMax: 3,
		range: 9,
		manaCost: 7,
		random: {
			damage: [4, 14],
			i_stunDuration: [6, 10],
		},
	},
	consecrate: {
		statType: 'int',
		statMult: 1,
		element: 'holy',
		cdMax: 15,
		castTimeMax: 4,
		manaCost: 12,
		range: 9,
		radius: 3,
		random: {
			healing: [0.3, 0.5],
			i_duration: [7, 13],
		},
	},

	'healing touch': {
		statType: 'int',
		statMult: 1,
		element: 'holy',
		cdMax: 5,
		castTimeMax: 3,
		manaCost: 8,
		range: 9,
		random: {
			healing: [1, 3],
		},
	},

	slash: {
		statType: 'str',
		statMult: 1,
		threatMult: 4,
		cdMax: 9,
		castTimeMax: 1,
		manaCost: 4,
		useWeaponRange: true,
		random: {
			damage: [6, 23],
		},
	},
	charge: {
		statType: 'str',
		statMult: 1,
		threatMult: 3,
		cdMax: 14,
		castTimeMax: 1,
		range: 10,
		manaCost: 3,
		random: {
			damage: [2, 11],
			i_stunDuration: [6, 10],
		},
	},
	flurry: {
		statType: 'dex',
		statMult: 1,
		cdMax: 20,
		castTimeMax: 0,
		manaCost: 10,
		random: {
			i_duration: [10, 20],
			i_chance: [30, 60],
		},
	},
	whirlwind: {
		statType: 'str',
		statMult: 1,
		threatMult: 6,
		cdMax: 12,
		castTimeMax: 2,
		manaCost: 7,
		random: {
			i_range: [1, 2.5],
			damage: [4, 18],
		},
	},
	smokebomb: {
		statType: 'dex',
		statMult: 1,
		element: 'poison',
		cdMax: 7,
		castTimeMax: 0,
		manaCost: 6,
		random: {
			damage: [0.25, 1.2],
			i_radius: [1, 3],
			i_duration: [7, 13],
		},
	},
	ambush: {
		statType: 'dex',
		statMult: 1,
		cdMax: 15,
		castTimeMax: 3,
		range: 10,
		manaCost: 7,
		random: {
			damage: [8, 35],
			i_stunDuration: [4, 7],
		},
	},
	'crystal spikes': {
		statType: ['dex', 'int'],
		statMult: 1,
		manaCost: 14,
		needLos: true,
		cdMax: 15,
		castTimeMax: 0,
		range: 9,
		isAttack: true,
		random: {
			damage: [3, 18],
			i_delay: [1, 4],
		},
		negativeStats: ['i_delay'],
	},
	innervation: {
		statType: ['str'],
		statMult: 1,
		manaReserve: {
			percentage: 0.25,
		},
		cdMax: 10,
		castTimeMax: 0,
		auraRange: 9,
		effect: 'regenHp',
		random: {
			regenPercentage: [0.3, 1.5],
		},
	},
	tranquility: {
		statType: ['int'],
		statMult: 1,
		element: 'holy',
		manaReserve: {
			percentage: 0.25,
		},
		cdMax: 10,
		castTimeMax: 0,
		auraRange: 9,
		effect: 'regenMana',
		random: {
			regenPercentage: [4, 10],
		},
	},
	swiftness: {
		statType: ['dex'],
		statMult: 1,
		element: 'fire',
		manaReserve: {
			percentage: 0.4,
		},
		cdMax: 10,
		castTimeMax: 0,
		auraRange: 9,
		effect: 'swiftness',
		random: {
			chance: [8, 20],
		},
	},
};

IWD_SPELLS['harvest life'] = {
	statType: ['str', 'int'],
	statMult: 1,
	cdMax: 10,
	castTimeMax: 3,
	manaCost: 5,
	isAttack: true,
	range: 1,
	random: {
		damage: [4, 14],
		healPercent: [2, 15],
	},
};

IWD_SPELLS['summon skeleton'] = {
	statType: ['str', 'int'],
	statMult: 0.27,
	cdMax: 6,
	castTimeMax: 6,
	manaCost: 5,
	range: 9,
	random: {
		damagePercent: [20, 76],
		hpPercent: [40, 60],
	},
};

IWD_SPELLS['blood barrier'] = {
	statType: ['str', 'int'],
	statMult: 0.1,
	cdMax: 13,
	castTimeMax: 3,
	manaCost: 5,
	range: 9,
	random: {
		i_drainPercentage: [10, 50],
		shieldMultiplier: [2, 5],
		i_frenzyDuration: [5, 15],
	},
};

const STAT_TRANSLATIONS: Record<string, string> = {
	vit: 'vitality',
	regenHp: 'health regeneration',
	manaMax: 'maximum mana',
	regenMana: 'mana regeneration',
	str: 'strength',
	int: 'intellect',
	dex: 'dexterity',
	armor: 'armor',

	blockAttackChance: 'chance to block attacks',
	blockSpellChance: 'chance to block spells',

	dodgeAttackChance: 'chance to dodge attacks',
	dodgeSpellChance: 'chance to dodge spells',

	addCritChance: 'global crit chance',
	addCritMultiplier: 'global crit multiplier',
	addAttackCritChance: 'attack crit chance',
	addAttackCritMultiplier: 'attack crit multiplier',
	addSpellCritChance: 'spell crit chance',
	addSpellCritMultiplier: 'spell crit multiplier',
	magicFind: 'increased item quality',
	itemQuantity: 'increased item quantity',
	sprintChance: 'sprint chance',
	allAttributes: 'to all attributes',
	xpIncrease: 'additional xp per kill',
	lvlRequire: 'level requirement reduction',

	elementArcanePercent: 'increased arcane damage',
	elementFrostPercent: 'increased frost damage',
	elementFirePercent: 'increased fire damage',
	elementHolyPercent: 'increased holy damage',
	elementPoisonPercent: 'increased poison damage',
	physicalPercent: 'increased physical damage',

	elementPercent: 'increased elemental damage',
	spellPercent: 'increased spell damage',

	elementAllResist: 'all resistance',
	elementArcaneResist: 'arcane resistance',
	elementFrostResist: 'frost resistance',
	elementFireResist: 'fire resistance',
	elementHolyResist: 'holy resistance',
	elementPoisonResist: 'poison resistance',

	attackSpeed: 'attack speed',
	castSpeed: 'cast speed',

	lifeOnHit: 'life gained on dealing physical damage',

	auraReserveMultiplier: 'aura mana reservation multiplier',

	//This stat is used for gambling when you can't see the stats
	stats: 'stats',

	//Fishing
	weight: 'lb',
	//Rods
	catchChance: 'extra catch chance',
	catchSpeed: 'faster catch speed',
	fishRarity: 'higher fish rarity',
	fishWeight: 'increased fish weight',
	fishItems: 'extra chance to hook items',
};

const SHORT_STAT_TRANSLATIONS: Record<string, string> = {
	vit: 'vitality',
	regenHp: 'hp regen',
	manaMax: 'max mana',
	regenMana: 'mana regen',
	str: 'strength',
	int: 'intellect',
	dex: 'dexterity',
	armor: 'armor',

	blockAttackChance: 'attack block',
	blockSpellChance: 'spell block',

	dodgeAttackChance: 'attack dodge',
	dodgeSpellChance: 'spell dodge',

	addCritChance: 'global crit %',
	addCritMultiplier: 'global crit mult',
	addAttackCritChance: 'attack crit %',
	addAttackCritMultiplier: 'attack crit mult',
	addSpellCritChance: 'spell crit %',
	addSpellCritMultiplier: 'spell crit mult',
	magicFind: 'item quality',
	itemQuantity: 'item quantity',
	sprintChance: 'sprint chance',
	allAttributes: 'all attributes',
	xpIncrease: 'xp increase',
	lvlRequire: 'lvl req reduction',

	elementArcanePercent: 'arcane damage',
	elementFrostPercent: 'frost damage',
	elementFirePercent: 'fire damage',
	elementHolyPercent: 'holy damage',
	elementPoisonPercent: 'poison damage',
	physicalPercent: 'physical damage',

	elementPercent: 'elemental damage',
	spellPercent: 'spell damage',

	elementAllResist: 'all res',
	elementArcaneResist: 'arcane res',
	elementFrostResist: 'frost res',
	elementFireResist: 'fire res',
	elementHolyResist: 'holy res',
	elementPoisonResist: 'poison res',

	attackSpeed: 'attack speed',
	castSpeed: 'cast speed',

	lifeOnHit: 'life gained on hit',

	auraReserveMultiplier: 'aura mana reservation multiplier',

	//This stat is used for gambling when you can't see the stats
	stats: 'stats',

	//Fishing
	weight: 'lb',
	//Rods
	catchChance: 'extra catch chance',
	catchSpeed: 'faster catch speed',
	fishRarity: 'higher fish rarity',
	fishWeight: 'increased fish weight',
	fishItems: 'extra chance to hook items',
};

const translate = (stat: string, settings = { shortenStatNames: false }) => {
	if (settings.shortenStatNames) {
		return SHORT_STAT_TRANSLATIONS[stat] ?? STAT_TRANSLATIONS[stat] ?? stat;
	} else {
		return STAT_TRANSLATIONS[stat] ?? stat;
	}
};

const percentageStats = [
	'addCritChance',
	'addCritemultiplier',
	'addAttackCritChance',
	'addAttackCritemultiplier',
	'addSpellCritChance',
	'addSpellCritemultiplier',
	'sprintChance',
	'xpIncrease',
	'blockAttackChance',
	'blockSpellChance',
	'dodgeAttackChance',
	'dodgeSpellChance',
	'attackSpeed',
	'castSpeed',
	'itemQuantity',
	'magicFind',
	'catchChance',
	'catchSpeed',
	'fishRarity',
	'fishWeight',
	'fishItems',
];

const stringifyStatValue = (statName: string, statValue: number) => {
	let res = statValue;
	if (statName.indexOf('CritChance') > -1) res = res / 20;

	let resStr = '' + res;

	if (
		percentageStats.includes(statName) ||
		statName.indexOf('Percent') > -1 ||
		(statName.indexOf('element') === 0 && statName.indexOf('Resist') === -1)
	)
		resStr += '%';

	return resStr;
};

const STAT_CALC = JSON.parse(
	'{"vit":{"stat":"vit","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,4],[1,4],[1,5],[1,6],[1,6],[1,7]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,6],[1,6],[1,7],[1,8],[1,9],[1,11],[1,12],[1,14]]},"regenHp":{"stat":"regenHp","range":[[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,6],[1,6],[1,7],[1,8],[1,9],[1,10],[1,11],[1,13],[1,14]],"range2h":[[1,1],[1,2],[1,2],[1,3],[1,4],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],[1,11],[1,12],[1,14],[1,15],[1,17],[1,20],[1,22],[1,25],[1,28]]},"manaMax":{"stat":"manaMax","range":[[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8]],"range2h":[[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8],[1,8]]},"regenMana":{"stat":"regenMana","range":[[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5]],"range2h":[[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5],[1,5]]},"str":{"stat":"str","range":[[1,1],[1,1],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,5],[1,5],[1,6],[1,6],[1,7],[1,7],[1,8],[1,8],[1,9],[1,9],[1,10]],"range2h":[[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],[1,10],[1,10],[1,11],[1,12],[1,13],[1,14],[1,15],[1,16],[1,17],[1,18],[1,19]]},"int":{"stat":"int","range":[[1,1],[1,1],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,5],[1,5],[1,6],[1,6],[1,7],[1,7],[1,8],[1,8],[1,9],[1,9],[1,10]],"range2h":[[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],[1,10],[1,10],[1,11],[1,12],[1,13],[1,14],[1,15],[1,16],[1,17],[1,18],[1,19]]},"dex":{"stat":"dex","range":[[1,1],[1,1],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,5],[1,5],[1,6],[1,6],[1,7],[1,7],[1,8],[1,8],[1,9],[1,9],[1,10]],"range2h":[[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],[1,10],[1,10],[1,11],[1,12],[1,13],[1,14],[1,15],[1,16],[1,17],[1,18],[1,19]]},"allAttributes":{"stat":"allAttributes","range":[[1,1],[1,1],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,5],[1,5],[1,6],[1,6],[1,7],[1,7],[1,8],[1,8],[1,9],[1,9],[1,10]],"range2h":[[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],[1,10],[1,10],[1,11],[1,12],[1,13],[1,14],[1,15],[1,16],[1,17],[1,18],[1,19]]},"attackSpeed":{"stat":"attackSpeed","range":[[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9]],"range2h":[[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9]]},"castSpeed":{"stat":"castSpeed","range":[[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9]],"range2h":[[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9],[1,9]]},"lifeOnHit":{"stat":"lifeOnHit","range":[[2,2],[2,2],[2,3],[2,3],[2,4],[2,4],[2,5],[2,5],[2,6],[2,6],[2,6],[2,7],[2,7],[2,8],[2,8],[2,9],[2,9],[2,10],[2,10],[2,10]],"range2h":[[2,2],[2,2],[2,3],[2,3],[2,4],[2,4],[2,5],[2,5],[2,6],[2,6],[2,6],[2,7],[2,7],[2,8],[2,8],[2,9],[2,9],[2,10],[2,10],[2,10]]},"blockAttackChance":{"stat":"blockAttackChance","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]]},"blockSpellChance":{"stat":"blockSpellChance","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]]},"dodgeAttackChance":{"stat":"dodgeAttackChance","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]]},"dodgeSpellChance":{"stat":"dodgeSpellChance","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]]},"magicFind":{"stat":"magicFind","range":[[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15]],"range2h":[[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15],[1,15]]},"itemQuantity":{"stat":"itemQuantity","range":[[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27]],"range2h":[[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27],[2,27]]},"xpIncrease":{"stat":"xpIncrease","range":[[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6]],"range2h":[[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6],[1,6]]},"sprintChance":{"stat":"sprintChance","range":[[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20]],"range2h":[[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20],[1,20]]},"lvlRequire":{"stat":"lvlRequire","range":[[1,0],[1,1],[1,1],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,5],[1,6],[1,6],[1,7],[1,7],[1,8],[1,8],[1,9],[1,9],[1,10]],"range2h":[[1,0],[1,1],[1,1],[1,2],[1,2],[1,3],[1,3],[1,4],[1,4],[1,5],[1,5],[1,6],[1,6],[1,7],[1,7],[1,8],[1,8],[1,9],[1,9],[1,10]],"level":{"min":5}},"addCritChance":{"stat":"addCritChance","range":[[1,-9],[1,-4],[1,0],[1,5],[1,10],[1,14],[1,19],[1,23],[1,28],[1,32],[1,37],[1,41],[1,46],[1,50],[1,55],[1,60],[1,64],[1,69],[1,73],[1,78]],"range2h":[[1,-18],[1,-9],[1,0],[1,10],[1,19],[1,28],[1,37],[1,46],[1,55],[1,64],[1,73],[1,82],[1,91],[1,100],[1,110],[1,119],[1,128],[1,137],[1,146],[1,155]],"level":{"min":7}},"addAttackCritChance":{"stat":"addAttackCritChance","range":[[1,-9],[1,-4],[1,0],[1,5],[1,10],[1,14],[1,19],[1,23],[1,28],[1,32],[1,37],[1,41],[1,46],[1,50],[1,55],[1,60],[1,64],[1,69],[1,73],[1,78]],"range2h":[[1,-18],[1,-9],[1,0],[1,10],[1,19],[1,28],[1,37],[1,46],[1,55],[1,64],[1,73],[1,82],[1,91],[1,100],[1,110],[1,119],[1,128],[1,137],[1,146],[1,155]],"level":{"min":7}},"addSpellCritChance":{"stat":"addSpellCritChance","range":[[1,-9],[1,-4],[1,0],[1,5],[1,10],[1,14],[1,19],[1,23],[1,28],[1,32],[1,37],[1,41],[1,46],[1,50],[1,55],[1,60],[1,64],[1,69],[1,73],[1,78]],"range2h":[[1,-18],[1,-9],[1,0],[1,10],[1,19],[1,28],[1,37],[1,46],[1,55],[1,64],[1,73],[1,82],[1,91],[1,100],[1,110],[1,119],[1,128],[1,137],[1,146],[1,155]],"level":{"min":7}},"elementArcanePercent":{"stat":"elementArcanePercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"elementFrostPercent":{"stat":"elementFrostPercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"elementFirePercent":{"stat":"elementFirePercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"elementHolyPercent":{"stat":"elementHolyPercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"elementPoisonPercent":{"stat":"elementPoisonPercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"physicalPercent":{"stat":"physicalPercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"elementPercent":{"stat":"elementPercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"spellPercent":{"stat":"spellPercent","range":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"range2h":[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3],[1,3]],"level":{"min":10}},"addCritMultiplier":{"stat":"addCritMultiplier","range":[[1,2],[1,3],[1,5],[1,6],[1,7],[1,9],[1,10],[1,11],[1,13],[1,14],[1,15],[1,17],[1,18],[1,20],[1,21],[1,22],[1,24],[1,25],[1,26],[1,28]],"range2h":[[1,3],[1,6],[1,9],[1,11],[1,14],[1,17],[1,20],[1,22],[1,25],[1,28],[1,30],[1,33],[1,36],[1,39],[1,41],[1,44],[1,47],[1,50],[1,52],[1,55]],"level":{"min":12}},"addAttackCritMultiplier":{"stat":"addAttackCritMultiplier","range":[[1,2],[1,3],[1,5],[1,6],[1,7],[1,9],[1,10],[1,11],[1,13],[1,14],[1,15],[1,17],[1,18],[1,20],[1,21],[1,22],[1,24],[1,25],[1,26],[1,28]],"range2h":[[1,3],[1,6],[1,9],[1,11],[1,14],[1,17],[1,20],[1,22],[1,25],[1,28],[1,30],[1,33],[1,36],[1,39],[1,41],[1,44],[1,47],[1,50],[1,52],[1,55]],"level":{"min":12}},"addSpellCritMultiplier":{"stat":"addSpellCritMultiplier","range":[[1,2],[1,3],[1,5],[1,6],[1,7],[1,9],[1,10],[1,11],[1,13],[1,14],[1,15],[1,17],[1,18],[1,20],[1,21],[1,22],[1,24],[1,25],[1,26],[1,28]],"range2h":[[1,3],[1,6],[1,9],[1,11],[1,14],[1,17],[1,20],[1,22],[1,25],[1,28],[1,30],[1,33],[1,36],[1,39],[1,41],[1,44],[1,47],[1,50],[1,52],[1,55]],"level":{"min":12}},"elementArcaneResist":{"stat":"elementArcaneResist","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19]],"level":{"min":15}},"elementFrostResist":{"stat":"elementFrostResist","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19]],"level":{"min":15}},"elementFireResist":{"stat":"elementFireResist","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19]],"level":{"min":15}},"elementHolyResist":{"stat":"elementHolyResist","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19]],"level":{"min":15}},"elementPoisonResist":{"stat":"elementPoisonResist","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19]],"level":{"min":15}},"elementAllResist":{"stat":"elementAllResist","range":[[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10],[1,10]],"range2h":[[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19],[1,19]],"level":{"min":15}}}'
);

export {
	IWD_SLOTS,
	IWD_TYPES,
	IWD_SPELLS,
	STAT_CALC,
	STAT_TRANSLATIONS,
	translate,
	stringifyStatValue,
};
