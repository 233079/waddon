import React, { useEffect, useState } from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import { TICK_DURATION } from '../../config';
import hotkeys from '../../core/hotkeys';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import {
	IwdChatMessage,
	IwdClientModule,
	IwdEvents,
	IwdObject,
} from '../../isleward.types';
import { settings } from '../../settings';
import { getPlayer } from '../../util/getWindow';
import { requireAsync } from '../../util/requireAsync';
import { TimersPanel } from './TimersPanel';

import './styles.css';

// Load in init()
let client: IwdClientModule;

const CHANNEL_NAME = 'timers';

const IGNORED_ZONES = ['fjolgardHousing'];

const config = [
	{
		id: 'm',
		name: "M'ogresh",
		respawnAfter: 400,
	},
	{
		id: 's',
		name: 'Stinktooth',
		respawnAfter: 1714,
	},
	{
		id: 'r',
		name: 'Radulos',
		respawnAfter: 1714,
	},
];

// Track timestamps of next respawns
const timings: Record<string, number> = {};

// Subscriptions
let listeners: ((t: typeof timings) => void)[] = [];
const subscribe = (fn: (t: typeof timings) => void) => {
	listeners.push(fn);
};
const unsubscribe = (fn: (t: typeof timings) => void) => {
	listeners = listeners.filter((l) => l !== fn);
};
const notify = () => {
	listeners.forEach((l) => l(timings));

	// We call notify whenever timings change,
	// so let's also cache them here
	const old = settings.getValue('timers.cache');
	settings.setValue('timers.cache', { ...old, ...timings });
};
const useTimings = () => {
	const [data, setData] = useState<typeof timings>(() => timings);

	useEffect(() => {
		const handler = (d: typeof timings) => {
			setData(d);
		};

		subscribe(handler);

		return () => {
			unsubscribe(handler);
		};
	}, []);

	return data;
};

// Track IDs of timed enemies
const tracking: Record<number, string> = {};

const onGetObject = (obj: IwdObject) => {
	// Don't update timer if in an ignored zone
	const playerZoneId = getPlayer()?.zoneId;
	if (playerZoneId && IGNORED_ZONES.includes(playerZoneId)) {
		return;
	}

	if (obj.name) {
		const name = obj.name.toLowerCase();

		const timer = config.find((t) => t.name.toLowerCase() === name);

		if (timer) {
			tracking[obj.id] = timer.name;

			timings[timer.id] = Date.now();
			notify();

			// Synchronize up
			client.request({
				cpn: 'social',
				method: 'chat',
				data: {
					message: `${timer.id}=${Date.now()}`,
					type: 'custom',
					subType: 'timers',
				},
			});
		}
	}

	if (obj.destroyed && obj.destructionEvent === 'death') {
		if (tracking[obj.id]) {
			const timerName = tracking[obj.id];
			const timer = config.find((t) => t.name === timerName);

			if (!timer) return;

			const nextTime = Date.now() + timer.respawnAfter * TICK_DURATION;

			timings[timer.id] = nextTime;
			notify();

			// Synchronize down
			client.request({
				cpn: 'social',
				method: 'chat',
				data: {
					message: `${timer.id}=${nextTime}`,
					type: 'custom',
					subType: 'timers',
				},
			});
		}
	}
};

let syncTimeout: ReturnType<typeof setTimeout>;

let synced = false;
const trySync = () => {
	if (synced) return;

	syncTimeout = setTimeout(() => {
		synced = true;

		client.request({
			cpn: 'social',
			method: 'chat',
			data: {
				message: `sync`,
				type: 'custom',
				subType: 'timers',
			},
		});
	}, 3000);
};
const onShowCharacterSelect = () => {
	clearTimeout(syncTimeout);
};

const onGetMessages = ({ messages }: { messages: IwdChatMessage[] }) => {
	messages.forEach((m) => {
		if (m?.channel === CHANNEL_NAME) {
			const rawMessageParts = m.message.split(':');
			const rawMessage =
				rawMessageParts[rawMessageParts.length - 1].trim();

			if (rawMessage === 'sync') {
				if (m.source === getPlayer()?.name) return;

				client.request({
					cpn: 'social',
					method: 'chat',
					data: {
						message: Object.keys(timings)
							.map((t) => `${t}=${timings[t]}`)
							.join('|'),
						type: 'custom',
						subType: 'timers',
					},
				});

				return;
			}

			const timerParts = rawMessage.split('|');
			timerParts.forEach((timerRaw) => {
				const parts = timerRaw.split('=');
				if (parts.length !== 2) return;

				const timer = config.find((t) => t.id === parts[0]);
				if (!timer) return;

				const parsed = parseInt(parts[1]);
				if (isNaN(parsed)) return;
				timings[timer.id] = parsed;
				notify();
			});
		}
	});
};

let checkChannelInterval: ReturnType<typeof setInterval>;

const tryAutoJoin = (client: IwdClientModule) => {
	const messages = $('.uiMessages').data('ui');

	if (!messages) return;

	// Stop checking
	clearInterval(checkChannelInterval);

	if (!messages.customChannels.includes(CHANNEL_NAME)) {
		// TODO: not working for some reason
		// sendMessage(
		// 	'[Waddon] joining timers channel...',
		// 	'color-yellowB',
		// 	'info'
		// );

		client.request({
			cpn: 'social',
			method: 'chat',
			data: {
				message: `/join ${CHANNEL_NAME}`,
				type: 'global',
				subType: null,
			},
		});
	}

	// Autohide
	setTimeout(() => {
		const filterBtn = $(
			'.uiMessages.active > .filters > .filter.channel[filter="timers"]'
		);
		if (filterBtn.hasClass('active')) {
			filterBtn.trigger('click');
		}
	}, 1000);
};

const init = async () => {
	client = await requireAsync('js/system/client');
	const events: IwdEvents = await requireAsync('js/system/events');

	checkChannelInterval = setInterval(() => {
		tryAutoJoin(client);
	}, 500);

	hotkeys.register('t', () =>
		settings.transactValue('timers.enabled', (x) => !x)
	);

	menu.addElement({
		key: 'timers panel',
		category: 'main',
		el: (
			<ToggleSetting setting={'timers.enabled'}>
				timers panel
			</ToggleSetting>
		),
	});

	renderer.addElement('timers-panel', <TimersPanel />);

	events.on('onGetObject', onGetObject);
	events.on('onGetMessages', onGetMessages);

	events.on('onBuildIngameUis', trySync);
	events.on('onShowCharacterSelect', onShowCharacterSelect);
};

export default { init, useTimings, config };
