import { IwdChatMessage, IwdConnectedPlayer } from '../../isleward.types';
import { settings } from '../../settings';
import { getWindow } from '../../util/getWindow';
import { sendPostRequest } from '../../util/sendHttpRequest';

const triggers = [
	/(\w+) has come online/,
	/(\w+) has gone offline/,
	/(\w+) has joined the party/,
	/(\w+) has left the party/,
];

const onGetMessages = ({ messages }: { messages: IwdChatMessage[] }) => {
	const format = settings.getValue('islebuilds.chatNames');
	if (format == 0) return;

	if (!messages) return;

	messages.forEach((m) => {
		if (!m.message) return;

		if (m.source) {
			const displayName = getCachedName(m.source);
			if (!displayName) return;

			if (displayName) {
				const parts = m.message.split(':');
				const idx = m.message.startsWith('(party:') ? 1 : 0;
				parts[idx] = parts[idx].replace(
					m.source,
					formatName(m.source, displayName)
				);
				m.message = parts.join(':');
			}
		} else if (m.message && !m.message.includes(':')) {
			for (let i = 0; i < triggers.length; i++) {
				const pattern = triggers[i];
				const found = m.message.match(pattern);
				if (!found) continue;

				m.message = m.message.replace(
					pattern,
					(match, p1, offset, string) => {
						const displayName = getCachedName(p1);
						const replacement = displayName
							? formatName(p1, displayName)
							: p1;

						return string.replace(p1, replacement);
					}
				);
			}
		}
	});
};

const formatName = (charName: string, displayName: string) => {
	const format = settings.getValue('islebuilds.chatNames');

	if (format == 0) {
		return charName;
	} else if (format == 1) {
		return `${charName} (${displayName})`;
	} else {
		return `<span onMouseOver="this.innerHTML='${charName}'" onMouseOut="this.innerHTML='${displayName}'">${displayName}</span><span class="color-grayA">*</span>`;
	}
};

const getCachedName = (char: string) => {
	const cache = settings.getValue('islebuilds.chatNamesCache');

	return cache[char];
};

const characterBuffer: string[] = [];
let fetchDebounce: ReturnType<typeof setTimeout> | null = null;
const debounceTimeout = 2000;
const queueFetchName = (character: string) => {
	characterBuffer.push(character);

	// Debounce call to fetchNames
	if (fetchDebounce) {
		clearTimeout(fetchDebounce);
	}
	fetchDebounce = setTimeout(fetchNames, debounceTimeout);
};

const fetchNames = async () => {
	const list = characterBuffer.splice(0);

	const response = await sendPostRequest('/api/nicknames', {
		characters: list,
	});
	const data = JSON.parse(response);

	const cache = settings.getValue('islebuilds.chatNamesCache');
	settings.setValue('islebuilds.chatNamesCache', { ...cache, ...data });
};

const onGetConnectedPlayer = (
	playerOrList: IwdConnectedPlayer | IwdConnectedPlayer[]
) => {
	const list = Array.isArray(playerOrList) ? playerOrList : [playerOrList];

	list.forEach((c) => queueFetchName(c.name));
};

const onAfterRenderUi = ({ ui }: any) => {
	if (ui.type === 'online') {
		// If we missed any events, we can get the existing list from the ui
		onGetConnectedPlayer(ui.onlineList);

		// We need to wait for uiOnline to register its listener first
		// See isleward/isleward#1958
		getWindow().addons?.events?.on(
			'onGetConnectedPlayer',
			onGetConnectedPlayer
		);
	}
};

export const nicknamesInit = () => {
	const format = settings.getValue('islebuilds.chatNames');
	if (!format) {
		return;
	}

	getWindow().addons?.events?.on('onGetMessages', onGetMessages);
	getWindow().addons?.events?.on('onAfterRenderUi', onAfterRenderUi);
};
