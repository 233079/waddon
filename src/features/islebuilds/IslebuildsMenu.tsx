import { CM } from '@kckckc/isleward-util';
import humanizeDuration from 'humanize-duration';
import React from 'react';
import { useGameState } from '../../core/state/useGameState';
import { useSetting } from '../../hooks/useSetting';
import {
	openIslebuildsPage,
	uploadCharacter,
} from '../islebuilds/islebuildsApi';
import { NicknamesButton } from './NicknamesButton';

const makeTimeButton = (
	setUploadPeriod: (arg: number | ((prev: number) => number)) => void,
	label: string,
	time: number
) => {
	return (
		<CM.Button
			onClick={() => {
				setUploadPeriod(time);
			}}
		>
			{label}
		</CM.Button>
	);
};

const makeTimePeriodName = (time: number) => {
	if (time === -1) return 'never';

	return '~' + humanizeDuration(time);
};

export const IslebuildsMenu: React.FC = () => {
	const [authToken, setAuthToken] = useSetting('islebuilds.authToken');
	const [uploadPeriod, setUploadPeriod] = useSetting(
		'islebuilds.uploadPeriod'
	);
	const [announceUploads, setAnnounceUploads] = useSetting(
		'islebuilds.announceUploads'
	);
	const [syncInventory, setSyncInventory] = useSetting(
		'islebuilds.syncInventory'
	);

	const gameState = useGameState();

	if (authToken === '') {
		// Not authed
		return (
			<>
				<NicknamesButton />
				<CM.Button
					onClick={() => {
						openIslebuildsPage('/waddon/connect');
					}}
				>
					[open setup]
				</CM.Button>
				<CM.Button
					onClick={() => {
						const token = prompt('Enter islebuilds token:', '');
						setAuthToken(token ?? '');
					}}
				>
					[set token manually]
				</CM.Button>
			</>
		);
	} else {
		// Authed
		return (
			<>
				<NicknamesButton />

				{/* Upload and open page button */}
				{gameState === 'game' && (
					<CM.Button
						onClick={() => {
							uploadCharacter();
						}}
					>
						[upload &amp; view character]
					</CM.Button>
				)}

				{/* Upload interval settings */}
				<CM.Expand
					label={`upload every: ${makeTimePeriodName(uploadPeriod)}`}
				>
					{makeTimeButton(setUploadPeriod, 'never', -1)}
					{makeTimeButton(setUploadPeriod, '5 mins', 1000 * 60 * 5)}
					{makeTimeButton(setUploadPeriod, '10 mins', 1000 * 60 * 10)}
					{makeTimeButton(setUploadPeriod, '30 mins', 1000 * 60 * 30)}
					{makeTimeButton(setUploadPeriod, '1 hour', 1000 * 60 * 60)}
					{makeTimeButton(
						setUploadPeriod,
						'2 hours',
						1000 * 60 * 60 * 2
					)}
					{makeTimeButton(
						setUploadPeriod,
						'4 hours',
						1000 * 60 * 60 * 4
					)}
					{makeTimeButton(
						setUploadPeriod,
						'8 hours',
						1000 * 60 * 60 * 8
					)}
					{makeTimeButton(
						setUploadPeriod,
						'12 hours',
						1000 * 60 * 60 * 12
					)}
				</CM.Expand>

				{/* Announce setting */}
				<CM.Checkbox
					checked={announceUploads}
					onChange={() => {
						setAnnounceUploads((x) => !x);
					}}
				>
					announce uploads
				</CM.Checkbox>

				{/* Inventory setting */}
				<CM.Checkbox
					checked={syncInventory}
					onChange={() => {
						setSyncInventory((x) => !x);
					}}
				>
					sync inventory
				</CM.Checkbox>

				{/* sign out */}
				<CM.Button
					onClick={() => {
						setAuthToken('');
					}}
				>
					[remove token]
				</CM.Button>
			</>
		);
	}
};
