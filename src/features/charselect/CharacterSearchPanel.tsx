import { Autocomplete, Panel } from '@kckckc/isleward-util';
import React, { useEffect, useState } from 'react';
import { playCharacter } from '.';

const useCharactersList = () => {
	const [list, setList] = useState<string[]>([]);
	const [redraw, setRedraw] = useState({});

	useEffect(() => {
		const ui = $('.uiCharacters').data('ui');
		if (!ui?.characters?.length) {
			setTimeout(() => {
				setRedraw({});
			}, 100);
		} else {
			setList(ui.characters.map((c: any) => c.name));
		}
	}, [redraw]);

	return list;
};

export const CharacterSearchPanel: React.FC = () => {
	const [value, setValue] = useState('');
	// const [locked, setLocked] = useState(false);
	const options = useCharactersList();

	return (
		<Panel
			saveAs="waddon.charselect"
			header="Character Search"
			// openCentered
		>
			<div style={{ width: '50vw', padding: '8px' }}>
				<Autocomplete
					id={'char-search'}
					value={value}
					setValue={setValue}
					options={options}
					selectMode={true}
					onSubmit={() => {
						// if (locked) return;

						// setLocked(true);

						playCharacter(value.trim());
					}}
				/>
			</div>
		</Panel>
	);
};
