import React from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import menu from '../../core/menu';
import renderer from '../../core/renderer';
import stateModule, { GameState } from '../../core/state';
import { IwdClientModule } from '../../isleward.types';
import { settings } from '../../settings';
import { requireAsync } from '../../util/requireAsync';
import { CharacterSearchPanel } from './CharacterSearchPanel';

let client: IwdClientModule;

const onStateChanged = (state: GameState) => {
	renderer.removeElement('charselect-panel');

	if (state === 'select') {
		const enabled = settings.getValue('charselect.enabled');
		if (!enabled) return;

		renderer.addElement('charselect-panel', <CharacterSearchPanel />);
	}
};

export const playCharacter = (name: string) => {
	client.request({
		cpn: 'auth',
		method: 'getCharacter',
		data: {
			name: name,
		},
		callback: () => {
			client.request({
				cpn: 'auth',
				method: 'play',
				data: {
					name: name,
				},
				callback: () => {
					const ui = $('.uiCharacters').data('ui');
					ui.destroy();
				},
			});
		},
	});
};

const init = async () => {
	client = await requireAsync('js/system/client');

	menu.addElement({
		key: 'character search',
		category: 'qol',
		el: (
			<ToggleSetting setting={'charselect.enabled'}>
				character search [experimental]
			</ToggleSetting>
		),
	});

	stateModule.subscribe(onStateChanged);
};

export default { init };
