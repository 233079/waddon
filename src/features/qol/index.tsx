import React from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';
import menu from '../../core/menu';
import stateModule from '../../core/state';
import { IwdObject } from '../../isleward.types';
import { settings } from '../../settings';
import { getPlayer, getWindow } from '../../util/getWindow';
import { requireAsync } from '../../util/requireAsync';
import './styles.css';

let uiFactory: any;
let client: any;

const applyHideQuests = () => {
	const player = getPlayer();
	const els = document.getElementsByClassName('uiQuests');

	if (!player || els.length < 1) {
		setTimeout(() => {
			applyHideQuests();
		}, 500);
		return;
	}

	const el = els[0];

	const value = settings.getValue('qol.hideQuests');

	if (value) {
		if (player?.level === 20) {
			if (el.className.indexOf('waddonHideQuests') === -1) {
				el.className += ' waddonHideQuests';
			}
		}
	} else {
		if (el.className.indexOf('waddonHideQuests') > -1) {
			el.className = el.className.replace('waddonHideQuests', '');
		}
	}
};

const onBeforePlayerContext = (target: IwdObject, context: any) => {
	const value = settings.getValue('qol.playerContext');
	if (!value) return;

	const events = getWindow()?.addons?.events;

	context.push({
		text: 'whisper',
		callback: () => {
			if (!target.name) return;

			// Open whisper in chat
			events?.emit('onDoWhisper', target.name);
		},
	});

	context.push({
		text: 'invite to party',
		callback: () => {
			// Get online list from uiFactory
			const onlineList = uiFactory.uis.find(
				(u: any) => u.type === 'online'
			).onlineList;
			// Find online user for serverId
			const onlineUser = onlineList.find(
				(p: any) => p.name === target.name
			);
			// Send invite
			if (onlineUser) {
				client.request({
					cpn: 'social',
					method: 'getInvite',
					data: {
						targetId: onlineUser.id,
					},
				});
			}
		},
	});
};

const init = async () => {
	const events = getWindow()?.addons?.events;
	uiFactory = await requireAsync('ui/factory');
	client = await requireAsync('js/system/client');

	menu.addElement({
		key: 'hide quests',
		category: 'qol',
		el: (
			<ToggleSetting setting={'qol.hideQuests'}>
				hide quests at 20
			</ToggleSetting>
		),
	});
	menu.addElement({
		key: 'player context menu',
		category: 'qol',
		el: (
			<ToggleSetting setting={'qol.playerContext'}>
				player context menu
			</ToggleSetting>
		),
	});

	events?.on('onBeforePlayerContext', onBeforePlayerContext);

	settings.onKeyChanged('qol.hideQuests', applyHideQuests);

	stateModule.subscribe(applyHideQuests);

	applyHideQuests();
};

export default { init };
