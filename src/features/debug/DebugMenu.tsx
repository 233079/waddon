import React from 'react';
import { ToggleSetting } from '../../components/ToggleSetting';

export const DebugMenu: React.FC = () => {
	return (
		<>
			<ToggleSetting setting="addonMenu.misc.logLinkedItems">
				Log linked items
			</ToggleSetting>
		</>
	);
};
