import React from 'react';
import { IwdEvents } from '../../isleward.types';
import { settings } from '../../settings';
import { getWindow } from '../../util/getWindow';
import menu from '../../core/menu';
import { DebugMenu } from './DebugMenu';

const ignoreList: string[] = [
	// 'onResize',
	// 'onAfterRenderUi',
	// 'onUiHover',
	// 'mouseDown',
	// 'mouseUp',
];

const logEvents = false;
const enableLogEvents = (events: IwdEvents) => {
	events.emit = new Proxy(events.emit, {
		apply: function (target, thisArg, argumentsList) {
			// Skip some events we don't care about
			if (!ignoreList.includes(argumentsList[0])) {
				// Logging events
				console.log('Emit', argumentsList[0], argumentsList);
			}

			// Continue to emit it like before
			return target.apply(thisArg, argumentsList);
		},
	});
};

// Log Linked Items
let logLinkedItems = false;
const onGetMessages = (e: any) => {
	let messages = e.messages;

	if (!Array.isArray(messages)) {
		messages = [messages];
	}

	messages.forEach((m: any) => {
		if (m.item && logLinkedItems) {
			const source = m.source ? `${m.source} linked item` : 'looted item';

			console.log(`${source}:`, m.item);
		}
	});
};

const init = () => {
	const events = getWindow().addons?.events;
	if (!events) return;

	menu.addElement({
		key: 'debug',
		el: <DebugMenu />,
	});

	events.on('onGetMessages', onGetMessages);

	settings.onKeyChanged('addonMenu.misc.logLinkedItems', (s) => {
		logLinkedItems = s;
	});

	if (logEvents) {
		enableLogEvents(events);
	}
};

export default {
	init,
};
