import { CM, Panel } from '@kckckc/isleward-util';
import React, { MouseEvent, useEffect, useRef } from 'react';
import { useGameState } from '../../core/state/useGameState';
import { useEvents } from '../../hooks/useEvents';
import { useSetting } from '../../hooks/useSetting';
import {
	drawMap,
	drawObjects,
	getDimensions,
	getTitle,
	useMinimapData,
} from './index';
import { RULESET } from './rules';

export const MinimapPanel = () => {
	const [minimapEnabled] = useSetting('minimap.enabled');
	const [minimapScale, setMinimapScale] = useSetting('minimap.scale');
	const [minimapRules, setMinimapRules] = useSetting('minimap.rules');
	const [minimapLogObjects, setMinimapLogObjects] =
		useSetting('minimap.logObjects');

	const events = useEvents();
	const gameState = useGameState();

	const data = useMinimapData();

	// Canvas ref
	const canvasRef = useRef<HTMLCanvasElement>(null);
	useEffect(() => {
		if (!minimapEnabled) return;

		const canvas = canvasRef.current;
		if (!canvas) return;
		const context = canvas.getContext('2d');
		if (!context) return;

		const dim = getDimensions();
		if (!dim) return;

		const { width, height } = dim;

		canvas.width = width;
		canvas.height = height;

		canvas.style.width = width * minimapScale + 'px';
		canvas.style.height = height * minimapScale + 'px';

		// Use a cached drawing of the base map and redraw the objects on top of it
		drawMap(context);

		drawObjects(context);
	}, [data, minimapRules, minimapScale, minimapEnabled]);

	const onMouseDown = (e: MouseEvent) => {
		const canvas = canvasRef.current;
		if (!canvas) return;

		const rect = canvas.getBoundingClientRect();
		const scaleX = canvas.width / rect.width;
		const scaleY = canvas.height / rect.height;
		const x = (e.clientX - rect.left) * scaleX;
		const y = (e.clientY - rect.top) * scaleY;

		events?.emit('waddon:onMinimapClicked', { x, y });
	};

	if (!minimapEnabled || gameState !== 'game') return null;

	return (
		<Panel
			saveAs="waddon.minimap"
			header={getTitle()}
			contextMenu={
				<CM>
					<CM.Expand label={`scale: ${minimapScale}`}>
						<CM.Button
							onClick={() => {
								const num = parseFloat(
									prompt('New minimap scale:') ?? '1'
								);
								if (isNaN(num) || num <= 0 || !num) return;
								setMinimapScale(num);
							}}
						>
							[enter scale]
						</CM.Button>
						<CM.Button onClick={() => setMinimapScale(1)}>
							set scale: 1
						</CM.Button>
						<CM.Button onClick={() => setMinimapScale(1.5)}>
							set scale: 1.5
						</CM.Button>
						<CM.Button onClick={() => setMinimapScale(2)}>
							set scale: 2
						</CM.Button>
						<CM.Button onClick={() => setMinimapScale(3)}>
							set scale: 3
						</CM.Button>
						<CM.Button onClick={() => setMinimapScale(4)}>
							set scale: 4
						</CM.Button>
					</CM.Expand>
					<CM.Expand label={`object rules`}>
						{RULESET.map((r) => {
							return (
								<CM.Checkbox
									key={r.ruleId}
									checked={minimapRules[r.ruleId]}
									onChange={() => {
										setMinimapRules((prev) => {
											return {
												...prev,
												[r.ruleId]: !prev[r.ruleId],
											};
										});
									}}
								>
									{r.ruleName}
								</CM.Checkbox>
							);
						})}
						<CM.Button
							onClick={() => {
								setMinimapRules((prev) => {
									const newEnabled: Record<string, boolean> =
										{};
									for (const rule in prev) {
										newEnabled[rule] = false;
									}
									return newEnabled;
								});
							}}
						>
							all off
						</CM.Button>
						<CM.Button
							onClick={() => {
								setMinimapRules((prev) => {
									const newEnabled: Record<string, boolean> =
										{};
									for (const rule in prev) {
										newEnabled[rule] = true;
									}
									return newEnabled;
								});
							}}
						>
							all on
						</CM.Button>
					</CM.Expand>
					<CM.Checkbox
						checked={minimapLogObjects}
						onChange={() => {
							setMinimapLogObjects((x) => !x);
						}}
					>
						log objects
					</CM.Checkbox>
				</CM>
			}
		>
			<canvas
				ref={canvasRef}
				onMouseDown={onMouseDown}
				style={{ padding: '4px' }}
			/>
		</Panel>
	);
};
