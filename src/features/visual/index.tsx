import React from 'react';
import { IwdObject, IwdRendererModule } from '../../isleward.types';
import { settings } from '../../settings';
import { getPlayer, getWindow } from '../../util/getWindow';
import { requireAsync } from '../../util/requireAsync';
import menu from '../../core/menu';
import { VisualMenu } from './VisualMenu';

// For .waddonHidden class
import './global.css';

export const VISUAL_INITIAL: {
	layers: Record<string, boolean>;
	uis: Record<string, boolean>;
	showCursor: boolean;
	showPlayer: boolean;
} = {
	layers: {
		objects: true,
		mobs: true,
		characters: true,
		attacks: true,
		effects: true,
		particles: true,
		lightPatches: true,
		lightBeams: true,
		tileSprites: true,
		hiders: true,
	},
	uis: {
		uiQuests: true,
		uiEvents: true,
		uiHud: true,
		uiTooltips: true,
		uiOverlay: true,
		uiDeath: true,
		uiTarget: true,
		uiSpells: true,
		uiMessages: true,
		uiMenu: true,
		uiEffects: true,
		uiParty: true,
		uiAnnouncements: true,
		uiProgressBar: true,
	},
	showCursor: true,
	showPlayer: true,
};

let renderer: IwdRendererModule;

const applySettings = () => {
	if (!renderer) return;

	const s = settings.getValue('renderer');

	// Update renderer layer visibility
	for (const layer in s.layers) {
		if (!renderer.layers[layer]) return;

		renderer.layers[layer].visible = s.layers[layer];
	}

	// Update iwd UI visibility
	for (const ui in s.uis) {
		const elCollection = document.getElementsByClassName(ui);
		if (elCollection.length < 1) {
			continue;
		}
		const el = elCollection[0];

		if (s.uis[ui]) {
			if (el.className.indexOf('waddonHidden') > -1) {
				el.className = el.className.replace('waddonHidden', '');
			}
		} else {
			if (el.className.indexOf('waddonHidden') === -1) {
				el.className += ' waddonHidden';
			}
		}
	}

	// Update cursor visibility
	const player = getPlayer();
	if (player?.mouseMover?.sprite) {
		player.mouseMover.sprite.visible = s.showCursor;
	}

	if (player?.sprite) {
		player.sprite.visible = s.showPlayer;
	}
};

const onGetObject = (o: IwdObject) => {
	if (o.self) {
		//hack
		setTimeout(() => {
			applySettings();
		}, 100);
	}
};

const init = async () => {
	const events = getWindow()?.addons?.events;
	renderer = await requireAsync('js/rendering/renderer');

	menu.addElement({
		key: 'visual',
		el: <VisualMenu />,
	});

	events?.on('onGetObject', onGetObject);

	settings.onKeyChanged('renderer', applySettings);
};

const visualModule = {
	init,
};

export default visualModule;
