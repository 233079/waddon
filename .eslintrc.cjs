module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	env: {
		browser: true,
		es2020: true,
	},
	plugins: ['@typescript-eslint'],
	extends: [
		'eslint:recommended',
		'plugin:react/recommended',
		'plugin:react-hooks/recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier',
	],
	parserOptions: {
		ecmaVersion: 12,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true,
		},
	},
	globals: {
		unsafeWindow: 'writable',
		GM_xmlhttpRequest: 'readonly',
		$: 'readonly',
		__ADDONVERSION__: 'readonly',
	},
	rules: {
		'react/prop-types': 'off',
		'no-restricted-globals': ['error', 'unsafeWindow', 'window'],
	},
};
